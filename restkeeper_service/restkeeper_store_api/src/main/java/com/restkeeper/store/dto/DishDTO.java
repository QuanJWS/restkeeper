package com.restkeeper.store.dto;

import lombok.Data;

@Data
public class DishDTO{
    private String dishId;
    private int dishNumber;
    private String dishRemark;
}
