package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.TableArea;

import java.util.List;
import java.util.Map;

public interface ITableAreaService extends IService<TableArea>{

    public boolean add(TableArea area);

    IPage<TableArea> queryPage(long pageNo,long pageSize);

    public List<Map<String, Object>> listTableArea();

    boolean deleteArea(String areaId);

}
