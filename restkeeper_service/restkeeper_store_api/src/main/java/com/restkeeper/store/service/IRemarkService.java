package com.restkeeper.store.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.Remark;

import java.util.List;

/**
 * <p>
 * 备注信息 服务类
 * </p>
 */
public interface IRemarkService extends IService<Remark> {

    /**
     * 获取门店信息
     * @return
     */
    public List<Remark> getRemarks();

    /**
     * 修改门店信息
     * @param remarks
     * @return
     */
    public boolean updateRemarks(List<Remark> remarks);


    /**
     * 根据备注名称获取内容
     * @return
     */
    public List<String> getRemark(String remarkName);
}
