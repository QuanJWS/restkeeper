package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.DishCategory;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜品及套餐分类 服务类
 * </p>
 */
public interface IDishCategoryService extends IService<DishCategory> {
    /**
     * 添加
     * @param name
     * @param type
     * @return
     */
    boolean add(String name, int type);

    /**
     * 修改
     * @param id
     * @param categoryName
     * @return
     */
    boolean update(String id, String categoryName);


    /**
     * 分页
     * @param pageIndex
     * @param pageSize
     * @return
     */
    IPage<DishCategory> queryPage(Long pageIndex, Long pageSize);

    /**
     * 根据分类获取下拉列表
     * @param type
     * @return
     */
    List<DishCategory> queryByType(int type);


    /**
     * 分类删除及校验
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 根据分类获取下拉列表
     * @param type
     * @return
     */
    List<Map<String,Object>> findCategoryList(Integer type);

    /**
     * 获取所有菜品分类列表
     * @return
     */
    List<DishCategory> getAllCategory();
}
