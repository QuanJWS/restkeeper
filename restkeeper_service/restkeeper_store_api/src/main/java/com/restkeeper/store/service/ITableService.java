package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.dto.TablePanelDTO;
import com.restkeeper.store.entity.Table;

public interface ITableService extends IService<Table>{

    public boolean add(Table table);

    IPage<Table> queryPage(long pageNo,long pageSize);

    IPage<Table> queryPageByArea(String areaId,long pageNo,long pageSize);

    /**
     * 统计桌台状态
     * 0 空闲
     * 1 开桌
     * 2 锁定
     * @param areaId
     * @param status
     * @return
     */
    public Integer countTableByStatus(String areaId, Integer status);


//    /**
//     * 统计当前预期下是否还有桌台，如果还有不能删除
//     * @param areaId
//     * @return
//     */
//    long getCountByAreaId(String areaId);

    /**
     * 获取桌台及相关统计信息
     * @return
     */
    TablePanelDTO getTablePanel(String areaId);

   // boolean changeTable(String sourceTableId,String targetTableId);

    boolean changeTable(String orderId,String targetTableId);

    /**
     * 清台
     * @param tableId
     * @return
     */
    boolean cleanTable(String tableId);

    /**
     * 锁桌
     * @param tableId
     * @return
     */
     boolean lockTable(String tableId);

    /**
     * 释放桌台
     * @param tableId
     * @return
     */
     boolean releaseTable(String tableId);
}
