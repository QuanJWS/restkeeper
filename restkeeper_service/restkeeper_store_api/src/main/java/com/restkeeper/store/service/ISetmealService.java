package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.entity.SetmealDish;

import java.util.List;


/**
 * 套餐接口
 */
public interface ISetmealService extends IService<Setmeal>{
    boolean add(Setmeal setmeal, List<SetmealDish> setmealDishes);

    IPage<Setmeal> queryPage(Long pageNo, Long pageSize);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(List<String> ids);


    long getCountByCategory(String categoryId);

    IPage<Setmeal> searchByName(String name,Long pageNo, Long pageSize);

    boolean update(Setmeal setmeal, List<SetmealDish> setmealDishes);

    IPage<Setmeal> queryByCategory(String categoryId, Long pageNo, Long pageSize);

    /**
     * 批量停售
     * @param ids
     * @return
     */
    public boolean forbiddenSalesStatus(List<String> ids);

    /**
     * 批量启售
     * @param ids
     * @return
     */
    public boolean enabledSalesStatus(List<String> ids);

}
