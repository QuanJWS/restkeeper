package com.restkeeper.store.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.DishFlavor;

import java.util.List;

public interface IDishFlavorService extends IService<DishFlavor>{

    boolean physicalDelete(String dishId);

    public List<DishFlavor> getFlavor(String dishId);



}
