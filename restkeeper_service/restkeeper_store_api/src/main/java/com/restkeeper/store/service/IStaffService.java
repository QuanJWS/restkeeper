package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.Staff;
import com.restkeeper.utils.Result;

/**
 * <p>
 * 员工信息 服务类
 * </p>
 */
public interface IStaffService extends IService<Staff> {

    Result login(String shopId,String loginName,String loginPass);

    boolean addStaff(Staff staff);

    boolean updateStaff(Staff staff);

    IPage<Staff> search(long page,long pageSize,String name);

}
