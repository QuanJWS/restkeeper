package com.restkeeper.store.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SetmealDish", description="套餐")
@TableName(value = "t_setmeal_dish",resultMap = "BaseResultMap")
public class SetmealDish implements Serializable{
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private String id;
    @TableField(value = "setmeal_id")
    private String setmealId;
    @TableField(value = "dish_id")
    private String dishId;
    @TableField(value = "dish_name")
    private String dishName;
    @TableField(value = "dish_copies")
    private Integer dishCopies;
    @TableField(value = "t_order")
    private Integer index;
    @ApiModelProperty(value = "所属商户id")
    @TableField(value = "shop_id")
    protected String shopId;
    @ApiModelProperty(value = "门店id")
    protected String storeId;
    @ApiModelProperty(value = "最后更新时间")
    @TableField(value = "last_update_time",fill = FieldFill.INSERT_UPDATE)
    protected LocalDateTime lastUpdateTime;
    @TableField(exist = false)
    private Dish dish;
}
