package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.dto.AllDishDTO;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.DishCategory;
import com.restkeeper.store.entity.DishFlavor;

import java.util.List;
import java.util.Map;

public interface IDishService extends IService<Dish>{

    boolean save(Dish dish, List<DishFlavor> flavorList);

    boolean update(Dish dish, List<DishFlavor> flavorList);

    List<DishCategory> queryByType(int type);

    List<Dish> findByCode(String code);
    IPage<Dish> queryPage(Long pageNo, Long pageSize);
    IPage<Dish> queryByName(String name,Long pageNo, Long pageSize);
    IPage<Dish> queryByCategory(String categoryId,Long pageNo, Long pageSize);

    IPage<AllDishDTO> selectAllDish(long page, long pageSize);


//    /**
//     * 设置售卖状态
//     * @param ids
//     * @param status
//     * @return
//     */
//    boolean updateStatus(List<String> ids,Integer status);

    /**
     * 停售
     * @param ids
     * @return
     */
    boolean forbiddenSalesStatus(List<String> ids);


    /**
     * 启售
     * @param ids
     * @return
     */
    boolean enabledSalesStatus(List<String> ids);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean deleteDishs(List<String> ids);

    long getCountByCategory(String categoryId);

    boolean update(Dish dish, String flavorValue);

    List<Map<String,Object>> findEnableDishListInfo(String categoryId, String name);
}
