package com.restkeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.entity.Dictionary;

/**
 * <p>
 * 全局字典表 Mapper 接口
 * </p>
 */
public interface SysDictMapper extends BaseMapper<Dictionary> {

}
