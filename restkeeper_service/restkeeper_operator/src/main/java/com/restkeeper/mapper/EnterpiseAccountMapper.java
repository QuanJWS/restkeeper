package com.restkeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.entity.EnterpiseAccount;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 企业账号管理 Mapper 接口
 * </p>
 */
public interface EnterpiseAccountMapper extends BaseMapper<EnterpiseAccount> {

    @Update(value="update t_enterprise_account set is_deleted=0 where enterprise_id=#{pkid}")
    public boolean recovery(@Param("pkid") String id);
}
