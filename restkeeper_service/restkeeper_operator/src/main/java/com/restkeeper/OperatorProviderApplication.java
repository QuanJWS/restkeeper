package com.restkeeper;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.restkeeper.mapper")
@SpringBootApplication(scanBasePackages = {"com.restkeeper"})
public class OperatorProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OperatorProviderApplication.class, args);
    }
}
