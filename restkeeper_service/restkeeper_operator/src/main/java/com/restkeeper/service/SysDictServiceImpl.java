package com.restkeeper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.entity.Dictionary;
import com.restkeeper.mapper.SysDictMapper;
import org.apache.dubbo.config.annotation.Service;

import java.util.List;

/**
 * <p>
 * 全局字典表 服务实现类
 * </p>
 */
@Service(version = "1.0.0",protocol = "dubbo")
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, Dictionary> implements ISysDictService {

    @Override
    public List<Dictionary> getDictionaryList(String category) {
        QueryWrapper<Dictionary> qw = new QueryWrapper<>();
        qw.lambda()
                .eq(Dictionary::getCategory,category);
        return this.list(qw);
    }
}
