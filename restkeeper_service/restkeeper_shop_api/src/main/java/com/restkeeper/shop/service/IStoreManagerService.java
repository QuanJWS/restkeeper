package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.shop.entity.StoreManager;
import com.restkeeper.utils.Result;

import java.util.List;

/**
 * <p>
 * 门店管理员信息 服务类
 * </p>
 */
public interface IStoreManagerService extends IService<StoreManager> {

    public IPage page(String criteria,long page, long pageSize);

    /**
     * 门店管理员登录接口
     * @param shopId
     * @param phone
     * @param loginpass
     * @return
     */
    public Result login(String shopId, String phone, String loginpass);

    /**
     * 门店管理员添加逻辑
     * @param name
     * @param phone
     * @param storeIds
     * @return
     */
    public boolean addStoreManager(String name, String phone, List<String> storeIds);

    /**
     * 门店管理员切换逻辑
     * @param storeId
     * @param storeId
     * @return
     */
    public Result switchStore(String storeId);


    /**
     * 门店管理员修改
     * @param storeManagerId
     * @param name
     * @param phone
     * @param storeIds
     * @return
     */
    public boolean updateStoreManager(String storeManagerId, String name,String phone, List<String> storeIds);


    /**
     * 删除门店
     * @param storeManagerId
     * @return
     */
    public boolean deleteStoreManager(String storeManagerId);

    /**
     * 修改门店管理员状态
     * @param storeManagerId
     * @return
     */
    public boolean updateStatus(String storeManagerId, int status);

}
