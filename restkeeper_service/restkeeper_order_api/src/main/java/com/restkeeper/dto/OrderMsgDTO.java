package com.restkeeper.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderMsgDTO implements Serializable{
    private static final long serialVersionUID = 1L;
    private String orderId;
    private String shopId;
    private String storeId;
}
