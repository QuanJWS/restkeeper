package com.restkeeper.dto;

import lombok.Data;

import java.util.List;

/**
 * 加菜
 */
@Data
public class PlusDishDTO{
    private String orderId;
    private Integer payAmmount;
    private List<DishDTO> dishList;
}
