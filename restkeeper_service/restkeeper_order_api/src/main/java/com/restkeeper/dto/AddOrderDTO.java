package com.restkeeper.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AddOrderDTO implements Serializable{
    private static final long serialVersionUID = 1L;
    private String tableId;
    private Double totalAmount;
    private int personNumbers;
    private String remarks;
    private List<DishDTO> dishList;
}
