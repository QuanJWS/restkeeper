package com.restkeeper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.entity.OrderDetailAllView;
import com.restkeeper.entity.OrderDetailEntity;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 订单详情表 服务类
 * </p>
 */
public interface IOrderDetailService extends IService<OrderDetailEntity> {
    /**
     * 按销售额汇总当天菜品分类数据
     * @return
     */
    List<OrderDetailAllView> getCurrentCategoryAmountCollect(LocalDate start,LocalDate end);

    /**
     * 按销量汇总当天菜品分类数据
     * @return
     */
    List<OrderDetailAllView> getCurrentCategoryCountCollect(LocalDate start,LocalDate end);

    /**
     * 当日菜品销售排行
     * @return
     */
    List<OrderDetailAllView> getCurrentDishRank(LocalDate start,LocalDate end);

    /**
     * 根据orderId获取订单详情
     * @param orderId
     * @return
     */
    List<OrderDetailEntity> getDetailsByOrderId(String orderId);
}
