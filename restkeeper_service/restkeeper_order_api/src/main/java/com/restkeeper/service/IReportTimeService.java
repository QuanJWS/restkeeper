package com.restkeeper.service;

import com.restkeeper.dto.CurrentHourCollectDTO;

import java.time.LocalDate;
import java.util.List;

public interface IReportTimeService {

    List<CurrentHourCollectDTO> getDateHourCollect(LocalDate start,Integer type);
}
