package com.restkeeper.service;

public interface IWeiXinPay
{
    /**
     * 通过jsCode获取openId
     * @param jsCode
     * @return
     */
    String getOpenId(String jsCode);

    /**
     * 调用统一下单接口发起支付
     * @param openId
     * @param orderId
     * @return
     */
    String requestPay(String openId,String orderId);

    /**
     * 微信回调之后的处理
     * @param notifyResult
     * @throws Exception
     */
    String notify(String notifyResult) throws Exception;
}
