package com.restkeeper.service;

import com.restkeeper.entity.ReverseOrder;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 反结账主表 服务类
 * </p>
 */
public interface IReverseOrderService extends IService<ReverseOrder> {
    boolean reverse(ReverseOrder reverseOrder);
}
