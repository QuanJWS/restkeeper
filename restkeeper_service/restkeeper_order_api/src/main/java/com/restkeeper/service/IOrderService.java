package com.restkeeper.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.dto.*;
import com.restkeeper.entity.OrderEntity;
import com.restkeeper.entity.OrderViewEntity;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单主表 服务类
 * </p>
 */
public interface IOrderService extends IService<OrderEntity> {
    /**
     * 下单
     * @param orderEntity
     * @return
     */
    String addOrder(OrderEntity orderEntity);

    /**
     * 结账
     * @param orderEntity
     * @return
     */
    boolean pay(OrderEntity orderEntity);

    boolean plusDish(PlusDishDTO plusDishDTO);

    public OrderEntity getNoPayOrder(String tableId);
    boolean hasNoPay(String tableId);
    //boolean changeTable(String sourceTableId,String targetTableId);
    //boolean returnDish(OrderDetailEntity orderDetailEntity);

    public boolean changeTable(String tableId, String targetTableId);

    public boolean changeNumber(String tableId, Integer number);

    /**
     * 退菜
     * @param detailDTO
     * @return
     */
    public boolean returnDish(DetailDTO detailDTO);


    /**
     * 赠菜
     * @param detailDTO
     * @return
     */
    public boolean presentDish(DetailDTO detailDTO);


    /**
     * 获取(当日)汇总数据
     * @return
     */
    CurrentAmountCollectDTO getCurrentCollect(LocalDate start, LocalDate end);

    /**
     * 统计24小时销售数据
     * @param start
     * @param end
     * @param type 统计类型 1:销售额;2:销售数量
     * @return
     */
    List<CurrentHourCollectDTO> getCurrentHourCollect(LocalDate start,LocalDate end,Integer type);

    /**
     * 收款方式构成汇总
     * @param start
     * @param end
     * @return
     */
    List<PayTypeCollectDTO> getPayTypeCollect(LocalDate start,LocalDate end);

    /**
     * 优惠金额汇总
     * @param start
     * @param end
     * @return
     */
    PrivilegeDTO getPrivilegeCollect(LocalDate start,LocalDate end);


    public IPage<OrderViewEntity> queryAllOrder(ReceiptDTO receiptDTO);

    public Map<String,Integer>  receiptCount(ReceiptDTO receiptDTO);

    public List<String> getOperators();

    String addMicroOrder(OrderEntity orderEntity);

    void print(int printType,String orderId,boolean isFront);
}
