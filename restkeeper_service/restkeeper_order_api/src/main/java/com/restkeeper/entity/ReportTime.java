package com.restkeeper.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

@Data
@TableName(value = "t_report_time")
public class ReportTime {

    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    private LocalDate payDate;

    private int payTime;

    private int totalCount;

    private int totalAmount;

    private String shopId;

    private String storeId;
}
