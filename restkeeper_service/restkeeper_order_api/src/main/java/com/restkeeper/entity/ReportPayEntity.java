package com.restkeeper.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "t_report_pay")
public class ReportPayEntity extends  BaseEntity  implements Serializable{
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    private LocalDate payDate;
    private Integer payType;
    /**
     * 应收金额
     */
    private Integer totalAmount;
    /**
     * 赠送金额
     */
    private Integer presentAmount;
    /**
     * 抹零金额
     */
    private Integer smallAmount;
    /**
     * 免单金额
     */
    private Integer freeAmount;
    /**
     * 实收金额
     */
    private Integer payAmount;
    /**
     * 就餐人数
     */
    private Integer personNumbers;
    /**
     * 交易笔数
     */
    private Integer payCount;
}
