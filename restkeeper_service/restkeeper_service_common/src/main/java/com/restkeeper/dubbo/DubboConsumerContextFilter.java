package com.restkeeper.dubbo;


import com.restkeeper.tenant.TenantContext;
import com.restkeeper.utils.SystemCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

@Activate
@Slf4j
public class DubboConsumerContextFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        log.info(" DubboConsumerContextFilter {} {} ", TenantContext.getAttachments(), Thread.currentThread().getName());
        System.out.println("DubboConsumerContextFilter "+TenantContext.getAttachments());
//        if(SystemCode.USER_TYPE_SHOP.equals(TenantContext.getUserType())){
//            RpcContext.getContext().setAttachment("shopId", TenantContext.getShopId());
//        }else if(SystemCode.USER_TYPE_STORE_MANAGER.equals(TenantContext.getUserType())
//                ||SystemCode.USER_TYPE_STAFF.equals(TenantContext.getUserType())){
//            RpcContext.getContext().setAttachment("shopId", TenantContext.getShopId());
//            RpcContext.getContext().setAttachment("storeId", TenantContext.getStorId());
//        }
        RpcContext.getContext().setAttachment("shopId", TenantContext.getShopId());
        RpcContext.getContext().setAttachment("storeId", TenantContext.getStorId());
        return invoker.invoke(invocation);
    }

    private static boolean isEmpty(String str) {
        return str == null || str.length() == 0 ||"null".equals(str);
    }
}


