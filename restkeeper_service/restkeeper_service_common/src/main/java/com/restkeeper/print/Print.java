package com.restkeeper.print;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Print implements Serializable{
    private static final long serialVersionUID = 1;
    private PrintOrder printOrder;
    private int printType;
    /**
     * 是否是前台收银端打印
     */
    private boolean isFront;
    private List<String> dishIdes;
    private int dishNumber;

    private String storeId;
    private String shopId;
}
