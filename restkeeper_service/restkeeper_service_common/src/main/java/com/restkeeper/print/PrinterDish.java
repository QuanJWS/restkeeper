package com.restkeeper.print;

import lombok.Data;

@Data
public class PrinterDish{
    private String dishName;
    private Integer price;
    private Integer amount;
    private Integer dishNumber;
}
