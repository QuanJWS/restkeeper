package com.restkeeper.service;

import com.restkeeper.BaseTest;
import com.restkeeper.dto.DayAmountCollectDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReportPayServiceImplTest extends BaseTest{
  @Autowired
  private IReportPayService reportPayService;
  @Test
  public void getDayAmountCollect() {
    LocalDate start = LocalDate.of(2020,1,10);
    List<DayAmountCollectDTO> results = reportPayService.getDayAmountCollect(start,start.plusDays(4));
    System.out.println(0);
  }
}