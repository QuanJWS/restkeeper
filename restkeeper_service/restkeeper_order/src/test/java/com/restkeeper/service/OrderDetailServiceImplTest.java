package com.restkeeper.service;

import com.restkeeper.BaseTest;
import com.restkeeper.entity.OrderDetailAllView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderDetailServiceImplTest extends BaseTest{
  @Autowired
  private IOrderDetailService orderDetailService;
  @Test
  public void getCurrentCategoryAmountCollect() {
    LocalDate start = LocalDate.of(2020,1,9);
    LocalDate end = start.plusDays(1);
    List<OrderDetailAllView> result = orderDetailService.getCurrentCategoryAmountCollect(start,end);
    System.out.println(result);
  }

  @Test
  public void getCurrentCategoryCountCollect(){
    LocalDate start = LocalDate.of(2020,1,9);
    LocalDate end = start.plusDays(1);
    List<OrderDetailAllView> result = orderDetailService.getCurrentCategoryCountCollect(start,end);
    System.out.println(result);
  }

  @Test
  public void getCurrentDishRank(){
    LocalDate start = LocalDate.of(2020,1,9);
    LocalDate end = start.plusDays(1);
    List<OrderDetailAllView> result = orderDetailService.getCurrentDishRank(start,end);

    System.out.println(result);
  }
}