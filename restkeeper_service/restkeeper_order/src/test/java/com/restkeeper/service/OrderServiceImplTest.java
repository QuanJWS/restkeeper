package com.restkeeper.service;

import com.restkeeper.BaseTest;
import com.restkeeper.dto.CurrentAmountCollectDTO;
import com.restkeeper.dto.CurrentHourCollectDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceImplTest extends BaseTest{
  @Autowired
  private IOrderService orderService;
  @Test
  public void getCurrentCollect() {
    LocalDate start = LocalDate.of(2020,1,9);
    LocalDate end = start.plusDays(4);
    CurrentAmountCollectDTO result = orderService.getCurrentCollect(start,end);
    System.out.println(0);
  }

  @Test
  public void getCurrentHourCollect(){
    LocalDate start = LocalDate.of(2020,1,9);
    LocalDate end = start.plusDays(1);
    List<CurrentHourCollectDTO> result = orderService.getCurrentHourCollect(start,end,1);
    System.out.println(result);
  }
}