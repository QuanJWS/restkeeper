package com.restkeeper;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.restkeeper.dto.OrderMsgDTO;
import com.restkeeper.entity.OrderDetailEntity;
import com.restkeeper.entity.OrderEntity;
import com.restkeeper.service.IOrderDetailService;
import com.restkeeper.service.IOrderService;
import com.restkeeper.service.IOrderSetmealDishService;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.service.ISetmealService;
import com.restkeeper.tenant.TenantContext;
import com.restkeeper.utils.Calculator;
import com.restkeeper.utils.SystemCode;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderTest extends BaseTest {

    @Reference(version = "1.0.0", check=false)
    private IOrderService orderService;

    @Reference(version = "1.0.0", check=false)
    private IOrderDetailService orderDetailService;

    @Reference(version = "1.0.0", check=false)
    private ISetmealService setmealService;

    @Autowired
    private IOrderSetmealDishService orderSetmealDishService;

    @Test
    public void addOrder() {
        OrderEntity orderEntity =new OrderEntity();
        orderEntity.setOrderNumber(IdWorker.getIdStr());
        orderEntity.setTableId("1111");
        orderEntity.setTotalAmount(3005);
        orderEntity.setCreateTime(LocalDateTime.now());
        //操作人员id
        orderEntity.setOperatorName(TenantContext.getLoginUserName());
        List<OrderDetailEntity> orderDetailEntityList =new ArrayList<OrderDetailEntity>();
        for (int i = 0; i <3 ; i++) {
            OrderDetailEntity orderDetailEntity=new OrderDetailEntity();
            orderDetailEntity.setDishId("111");
            orderDetailEntity.setTableId(orderEntity.getTableId());
            orderDetailEntity.setOrderNumber(orderEntity.getOrderNumber());
            orderDetailEntity.setDishId("1111");
            orderDetailEntity.setDishType(SystemCode.DISH_TYPE_MORMAL);
            orderDetailEntity.setDishName("西红柿炒鸡蛋"+i);
            orderDetailEntity.setDishNumber(1);
            orderDetailEntity.setDishPrice(150);
            orderDetailEntityList.add(orderDetailEntity);
        }
        orderEntity.setOrderDetails(orderDetailEntityList);
        orderService.addOrder(orderEntity);
//        OrderEntity orderEntity= orderService.getById("1207146953709944834");
//        System.out.println(orderEntity.getOrderDetails());

        //OrderDetailEntity orderDetailEntity= orderDetailService.getById("1207146954318118914");
    }

    @Test
    public void testSelect(){
        orderService.list();
    }

    @Test
    public void getSetmeal(){
        Setmeal setmeal = setmealService.getById("6352890890");
        System.out.println(setmeal);
    }

    @Test
    public void processSetmealDish(){
        BigDecimal result = Calculator.getRate(4,5);
        int a = Calculator.getPercentResult(7,new BigDecimal("0.5"));
        OrderMsgDTO dto = new OrderMsgDTO();
        dto.setStoreId("1205130634395324417");
        dto.setShopId("test");
        dto.setOrderId("43534");
        orderSetmealDishService.process(JSON.toJSONString(dto));
    }

//    @Autowired
//    private OrderMapper orderMapper;
//    @Test
//    public void getOrder(){
//        OrderEntity orderEntity = orderMapper.selectById("1207146953709944834");
//        int a = 0;
//    }
}
