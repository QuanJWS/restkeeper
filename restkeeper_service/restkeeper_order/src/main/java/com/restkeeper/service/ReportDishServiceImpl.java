package com.restkeeper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.entity.ReportDishEntity;
import com.restkeeper.mapper.ReportDishMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@Service(version = "1.0.0", protocol = "dubbo")
public class ReportDishServiceImpl extends ServiceImpl<ReportDishMapper, ReportDishEntity> implements IReportDishService{
    @Override
    public List<ReportDishEntity> getCategoryAmountCollect(LocalDate start, LocalDate end) {

        QueryWrapper<ReportDishEntity> wrapper = new QueryWrapper<>();
        wrapper.select("category","sum(dish_number) as dish_number","sum(dish_money) as dish_money")
                .lambda().ge(ReportDishEntity::getPayDate,start).le(ReportDishEntity::getPayDate,end)
                .groupBy(ReportDishEntity::getCategory);

        return this.list(wrapper);
    }

    @Override
    public List<ReportDishEntity> getDishRank(LocalDate start, LocalDate end) {

        QueryWrapper<ReportDishEntity> wrapper = new QueryWrapper<>();

        wrapper.select("dish_name","sum(dish_number) as dish_number")
                .lambda().ge(ReportDishEntity::getPayDate,start).le(ReportDishEntity::getPayDate,end)
                .groupBy(ReportDishEntity::getDishName);
        return this.list(wrapper);
    }
}
