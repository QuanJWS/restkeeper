package com.restkeeper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.restkeeper.dto.*;
import com.restkeeper.entity.ReportPayEntity;
import com.restkeeper.mapper.ReportPayMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Service(version = "1.0.0", protocol = "dubbo")
public class ReportPayServiceImpl extends ServiceImpl<ReportPayMapper, ReportPayEntity> implements IReportPayService{
    @Override
    public List<DayAmountCollectDTO> getDayAmountCollect(LocalDate start, LocalDate end) {

        QueryWrapper<ReportPayEntity> wrapper = new QueryWrapper<>();
        wrapper.select("sum(pay_amount) as pay_amount","sum(pay_count) as pay_count","pay_date")
                .lambda().ge(ReportPayEntity::getPayDate,start).le(ReportPayEntity::getPayDate,end)
                .groupBy(ReportPayEntity::getPayDate)
                .orderByDesc(ReportPayEntity::getPayDate);
        List<ReportPayEntity> reportPayList = this.list(wrapper);


        List<DayAmountCollectDTO> fromQuery = reportPayList.stream().map(r -> {
            DayAmountCollectDTO dayAmountCollectDTO = new DayAmountCollectDTO();
            dayAmountCollectDTO.setDate(r.getPayDate());
            dayAmountCollectDTO.setTotalAmount(r.getPayAmount());
            dayAmountCollectDTO.setTotalCount(r.getPayCount());
            return dayAmountCollectDTO;
        }).collect(Collectors.toList());


        List<DayAmountCollectDTO> result = Lists.newArrayList();
        result.addAll(fromQuery);

        //得到日期天数差
        long daysCount = end.toEpochDay()-start.toEpochDay()+1;

        // for(int start;start<=daysCount;start++)
        Stream.iterate(start,i->i.plusDays(1)).limit(daysCount)
                .map(d->{
                    DayAmountCollectDTO dayAmountCollectDTO = new DayAmountCollectDTO();
                    dayAmountCollectDTO.setDate(d);
                    dayAmountCollectDTO.setTotalCount(0);
                    dayAmountCollectDTO.setTotalAmount(0);
                    return dayAmountCollectDTO;
                })
                .forEach(d->{
                    //将原有数据插入到集合中
                    if (!fromQuery.stream().anyMatch(item->item.getDate().isEqual(d.getDate()))){
                        result.add(d);
                    }
                });

        //result排序
        result.sort((a,b)->{
            if (a.getDate().isBefore(b.getDate())){
                return -1;
            }
            if (a.getDate().isAfter(b.getDate())){
                return 1;
            }
            return 0;
        });


        return result;
    }

    @Override
    public List<PayTypeCollectDTO> getPayTypeCollect(LocalDate start, LocalDate end) {

        QueryWrapper<ReportPayEntity> wrapper = new QueryWrapper<>();
        wrapper.select("sum(pay_amount) as total_amount","pay_type")
                .lambda().ge(ReportPayEntity::getPayDate,start).le(ReportPayEntity::getPayDate,end)
                .isNotNull(ReportPayEntity::getPayType)
                .groupBy(ReportPayEntity::getPayType);
        List<ReportPayEntity> reportPayList = this.list(wrapper);


        return reportPayList.stream().map(reportPay -> {
            PayTypeCollectDTO dto = new PayTypeCollectDTO();
            dto.setPayType(reportPay.getPayType());
            dto.setTotalCount(reportPay.getTotalAmount());
            dto.setPayName(PayType.getName(reportPay.getPayType()));
            return dto;
        }).collect(Collectors.toList());
    }

    @Override
    public PrivilegeDTO getPrivilegeCollectByDate(LocalDate start, LocalDate end) {
        QueryWrapper<ReportPayEntity> wrapper = new QueryWrapper<>();
        wrapper.select("sum(present_amount) as present_amount","sum(small_amount) as small_amount","sum(free_amount) as free_amount")
                .lambda().ge(ReportPayEntity::getPayDate,start).le(ReportPayEntity::getPayDate,end);
        ReportPayEntity reportPay = this.getOne(wrapper);

        PrivilegeDTO dto = new PrivilegeDTO();

        if (reportPay == null){
            dto.setFreeAmount(0);
            dto.setPresentAmount(0);
            dto.setSmallAmount(0);
        }else {
            dto.setFreeAmount(reportPay.getFreeAmount());
            dto.setPresentAmount(reportPay.getPresentAmount());
            dto.setSmallAmount(reportPay.getSmallAmount());
        }

        return dto;
    }

    @Override
    public CurrentAmountCollectDTO getDateAmount(LocalDate start, LocalDate end) {

        CurrentAmountCollectDTO result = new CurrentAmountCollectDTO();

        //获取实收金额
        QueryWrapper<ReportPayEntity> payAmountWrapper = new QueryWrapper<>();
        payAmountWrapper.select("SUM(pay_amount) as pay_amount")
                .lambda()
                .ge(ReportPayEntity::getPayDate,start)
                .lt(ReportPayEntity::getPayDate,end);
        ReportPayEntity payAmount = this.getOne(payAmountWrapper);
        result.setPayTotal(payAmount!=null?payAmount.getPayAmount():0);

        //获取未收金额
        //因为是查询历史订单，不存在未收金额，因此该值为0
        result.setNoPayTotal(0);

        //获取就餐人数
        QueryWrapper<ReportPayEntity> totalPersonWrapper = new QueryWrapper<>();
        totalPersonWrapper.select("SUM(person_numbers) as person_numbers")
                .lambda()
                .ge(ReportPayEntity::getPayDate,start)
                .lt(ReportPayEntity::getPayDate,end);
        ReportPayEntity totalPerson = this.getOne(totalPersonWrapper);
        result.setTotalPerson(totalPerson!=null?totalPerson.getPersonNumbers():0);

        return result;
    }
}
