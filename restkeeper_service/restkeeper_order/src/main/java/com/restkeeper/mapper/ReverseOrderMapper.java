package com.restkeeper.mapper;

import com.restkeeper.entity.ReverseOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 反结账主表 Mapper 接口
 * </p>
 */
public interface ReverseOrderMapper extends BaseMapper<ReverseOrder> {

}
