package com.restkeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.entity.OrderDetailEntity;
import com.restkeeper.entity.OrderEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * 订单主表 Mapper 接口
 * </p>
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {

}
