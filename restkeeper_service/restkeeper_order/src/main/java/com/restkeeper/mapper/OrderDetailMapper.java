package com.restkeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.entity.OrderDetailEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetailEntity> {

    @Select(value="select * from t_order_detail where order_id=#{orderId}")
    List<OrderDetailEntity> getByOrderId(@Param("orderId") String orderId);

}
