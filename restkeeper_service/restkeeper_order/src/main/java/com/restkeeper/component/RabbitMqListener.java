//package com.restkeeper.component;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.rabbitmq.client.Channel;
//import com.restkeeper.entity.OrderDetailEntity;
//import com.restkeeper.entity.OrderDetailMealEntity;
//import com.restkeeper.rabbitmq.MqDefinition;
//import com.restkeeper.service.IOrderDetailMealService;
//import com.restkeeper.service.IOrderDetailService;
//import com.restkeeper.store.entity.Dish;
//import com.restkeeper.store.service.ISetmealDishService;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.dubbo.config.annotation.Reference;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//@Slf4j
//@Component
//public class RabbitMqListener{
//    @Autowired
//    private IOrderDetailService orderDetailService;
//    @Autowired
//    private IOrderDetailMealService orderDetailMealService;
//    @Reference(version = "1.0.0", check=false)
//    private ISetmealDishService setmealDishService;
//
//    @RabbitListener(queues = MqDefinition.ORDER_PAY_QUEUE_NAME)
//    public void processPayOrder(Message msg, Channel channel){
//        String orderId = new String(msg.getBody());
//
//        QueryWrapper<OrderDetailEntity> wrapper = new QueryWrapper<>();
//        //从订单详情里查出所有套餐
//        wrapper.lambda()
//                .eq(OrderDetailEntity::getOrderId,orderId)
//                .eq(OrderDetailEntity::getDishType,2);
//        orderDetailService.list(wrapper)
//                .forEach(o->{
//                    float payPrice = o.getDishPrice();
//                    int dishNumber = o.getDishNumber();
//                    String setmealId = o.getDishId();
//                    //通过套餐详情查出所有菜品
//                    List<Dish> dishList = setmealDishService.getAllDishBySetmealId(setmealId);
//                    float allDishPrice = dishList.stream().map(Dish::getPrice).reduce(Integer::sum).get();
//                    float rate = payPrice/allDishPrice;
//                    OrderDetailMealEntity orderDetailMealEntity = new OrderDetailMealEntity();
//                    BeanUtils.copyProperties(o,orderDetailMealEntity);
//                    //根据菜品生成菜品订单套餐详情
//                    dishList.forEach(d->{
//                        orderDetailMealEntity.setDishId(d.getId());
//                        orderDetailMealEntity.setDishName(d.getName());
//                        orderDetailMealEntity.setDishNumber(dishNumber);
//                        orderDetailMealEntity.setDishAmount((int)(d.getPrice()*dishNumber*rate));
//                        orderDetailMealService.save(orderDetailMealEntity);
//                    });
//                });
//    }
//}
