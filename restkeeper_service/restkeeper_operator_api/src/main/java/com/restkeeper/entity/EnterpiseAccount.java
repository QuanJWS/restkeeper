package com.restkeeper.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 企业账号管理
 * </p>
 */
@Data
@TableName(value="t_enterprise_account")
@ApiModel(value="EnterpriseAccount对象", description="企业账号对象")
public class EnterpiseAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "企业id")
    @TableId(type=IdType.ASSIGN_ID)
    private String enterpriseId;

    @ApiModelProperty(value = "企业名称")
    private String enterpriseName;

    @JsonIgnore
    @ApiModelProperty(value = "密码（后台自动下发）")
    private String password;

    @ApiModelProperty(value = "商户号（下发生成）")
    private String shopId;

    @ApiModelProperty(value = "申请人")
    private String applicant;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "省")
    private String province;
    
    @ApiModelProperty(value = "市")
    private String city;
    
    @ApiModelProperty(value = "区")
    private String area;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "申请时间（当前时间，精准到秒）")
    private LocalDateTime applicationTime;

    @ApiModelProperty(value = "到期时间 (试用下是默认七天后到期，状态改成停用)")
    private LocalDateTime expireTime;

    @ApiModelProperty(value = "有效天数，前端展现使用")
    @TableField(exist=false)
    private Long validityDay;

    @ApiModelProperty(value = "状态(试用中0，正式1,停用-1)")
    private Integer status;

    @JsonIgnore
    @ApiModelProperty(value = "最后更新时间")
    //支持自动填充
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime lastUpdateTime;

    @JsonIgnore
    @ApiModelProperty(value = "是否删除 1是删除 0 未删除")
    //支持逻辑删除
    @TableLogic
    private Integer isDeleted;

    public Long getValidityDay() {
        //如果过期返回-1
//        if(expireTime.isAfter(LocalDateTime.now())){
//            return -1L;
//        }
        java.time.Duration duration = java.time.Duration.between(expireTime, applicationTime);
        validityDay= Math.abs(duration.toDays());
        return validityDay;
    }
}
