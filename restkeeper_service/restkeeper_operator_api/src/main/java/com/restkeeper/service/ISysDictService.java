package com.restkeeper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.entity.Dictionary;

import java.util.List;

public interface ISysDictService extends IService<Dictionary>{

    /**
     * 分类获取字典表信息
     * @param category
     * @return
     */
    public List<Dictionary> getDictionaryList(String category);
}
