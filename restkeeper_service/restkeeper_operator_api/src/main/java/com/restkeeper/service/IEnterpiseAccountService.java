package com.restkeeper.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.entity.EnterpiseAccount;
import com.restkeeper.utils.Result;

/**
 * <p>
 * 企业账号管理 服务类
 * </p>
 */
public interface IEnterpiseAccountService extends IService<EnterpiseAccount> {

    public boolean add(EnterpiseAccount account);

    public boolean resetpwd(String id, String password);

    public IPage<EnterpiseAccount> queryPageByName(int pageNo, int pageSise, String name);

    public boolean recovery(String id);


    /**
     * 集团管理登录接口
     * @param shopId
     * @param telphone
     * @param loginpass
     * @return
     */
    public Result login(String shopId, String telphone, String loginpass);

}
