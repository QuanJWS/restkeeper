package com.restkeeper.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.shop.entity.Store;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 门店信息账号 Mapper 接口
 * </p>
 */
@Mapper
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface StoreMapper extends BaseMapper<Store> {

    @Select(value="select * from t_store where store_manager_id=#{managerId} order by last_update_time desc")
	public List<Store> selectStoreByManager(@Param("managerId") String managerId);

    //获取有效的省份列表
    @Select(value="select DISTINCT(province) from t_store where status=1 and is_deleted=0")
    public List<String> getAllProvince();

//    //获取有效的省份列表
//    @Select(value="select DISTINCT(province) from t_store where status=1 and is_deleted=0")
//    public List<String> getAllProvinceByStoreId(@Param("managerId") String managerId);

    @Select("select count(1) from t_store where brand_id=#{brandId} and status=1 and is_deleted=0")
    Integer getStoreCount(String brandId);

    @Select("select count(distinct(city)) from t_store where brand_id=#{brandId} and status=1 and is_deleted=0")
    Integer getCityCount(String brandId);
}
