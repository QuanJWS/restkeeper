package com.restkeeper.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.redis.MybatisRedisCache;
import com.restkeeper.shop.entity.Brand;
import com.restkeeper.shop.entity.StoreManager;
import org.apache.ibatis.annotations.*;

/**
 * <p>
 * 门店管理员信息 Mapper 接口
 * </p>
 */
@Mapper
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface StoreManagerMapper extends BaseMapper<StoreManager> {



}
