package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.shop.dto.StoreDTO;
import com.restkeeper.shop.entity.Store;
import com.restkeeper.shop.mapper.StoreMapper;
import com.restkeeper.tenant.TenantContext;
import com.restkeeper.utils.BeanListUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 门店信息账号 服务实现类
 * </p>
 */
@Slf4j
@Component("storeService")
@Service(version = "1.0.0",protocol = "dubbo")
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements IStoreService {

    @Override
    public IPage<Store> queryPageByName(long pageNo, long pageSise, String name) {
        IPage<Store> page = new Page<Store>(pageNo, pageSise);
        QueryWrapper<Store> queryWrapper = new QueryWrapper<Store>();
        if (!StringUtils.isEmpty(name)) {
            queryWrapper.like("store_name", name);
        }
        return this.page(page, queryWrapper);
    }

    @Override
    public List<String> listAllProvince(){
        return getBaseMapper().getAllProvince();
    }

    @Override
    public List<StoreDTO> getStoreByProvince(String province) {
        QueryWrapper<Store> queryWrapper = new QueryWrapper<Store>();
        //正常营业状态
        queryWrapper.lambda().eq(Store::getStatus,1);
        if (!StringUtils.isEmpty(province)&&!"all".equalsIgnoreCase(province)) {
            queryWrapper.lambda().eq(Store::getProvince, province);
        }
        List<Store> list = this.list(queryWrapper);
        List<StoreDTO> list_dto;
        try {
           return list_dto = BeanListUtils.copy(list, StoreDTO.class);
        } catch (Exception e) {
           log.info("转换出错");
        }
        return new ArrayList<StoreDTO>();
    }

    @Override
    public List<StoreDTO> listManagerStores() {
        QueryWrapper<Store> queryWrapper = new QueryWrapper<Store>();
        queryWrapper.lambda().eq(Store::getStatus,1).
        eq(Store::getStoreManagerId,  RpcContext.getContext().getAttachment("loginUserId"));
        List<Store> list = this.list(queryWrapper);
        try {
            return BeanListUtils.copy(list,StoreDTO.class);
        } catch (Exception e) {
            log.info("转换出错");
    }
        return new ArrayList<>();
    }
}
