package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.shop.entity.Brand;
import com.restkeeper.shop.mapper.BrandMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 品牌管理 服务实现类
 * </p>
 */
@Component("brandService")
@Service(version = "1.0.0",protocol = "dubbo")
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {

    @Override
    public IPage<Brand> queryPage(Long pageNo, Long pageSise) {
        IPage<Brand> page = new Page<>(pageNo, pageSise);
        QueryWrapper<Brand> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Brand::getLastUpdateTime);
        return this.page(page,queryWrapper);
    }

    @Override
    public List<Map<String, String>> brandList() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("brand_id", "brand_name");
        return this.listMaps(queryWrapper);
    }
}
