package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.restkeeper.shop.entity.Brand;
import com.restkeeper.shop.entity.Store;
import com.restkeeper.shop.entity.StoreManager;
import com.restkeeper.tenant.TenantContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.RpcContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class StoreServiceTest extends BaseTest{

//    @Reference(version = "1.0.0", check=false)
//    private IStoreService storeService;

//    @Reference(version = "1.0.0", check=false)
//    private IBrandService brandService;

    @Autowired
    private IBrandService brandService;

    @Autowired
    private IStoreService storeService;

    @Reference(version = "1.0.0", check=false)
    private IStoreManagerService storeManagerService;

//    @Before
//    public void init(){
//        TenantContext.addAttachment("type","1");
//        TenantContext.addAttachment("shopId","test");
//        RpcContext.getContext().setAttachment("shopId", "test");
//    }
//
//    @Test
//    public void queryPage() {
//        IPage<Brand> result = brandService.queryPage(1,10);
//        Assert.assertTrue(true);
//    }

//    @Test
//    public void testCache(){
//        for (int i = 0; i <4 ; i++) {
//          //  StoreManager storeManager  = storeManagerService.getById("1205130634617622530");
//            StoreManager storeManager  = storeManagerService.getById("-1");
//            if(storeManager!=null){
//                List<Store> stores =  storeManager.getStores();
//                System.out.println(stores);
//            }
//        }
//    }

    @Test
    public void testPageCache(){
        IPage<StoreManager> page = new Page<StoreManager>(1, 10);
        QueryWrapper<StoreManager> queryWrapper = new QueryWrapper<StoreManager>();
//        if (!StringUtils.isEmpty(name)) {
//            queryWrapper.like("store_name", name);
//        }
        for (int i = 0; i <2 ; i++) {
            page = storeManagerService.page(page, queryWrapper);
        }
    }


//    @Test
//    public void testadd(){
//        Brand brand =new Brand();
//        brand.setBrandName("test");
//        brand.setLogo("http://baidu.com");
//        log.info("current {}",Thread.currentThread().getName());
//        brandService.save(brand);
//        Store store =new Store();
//        store.setStoreName("门店1");
//        store.setProvince("北京");
//        store.setCity("昌平区");
//        store.setArea("金燕龙大厦");
//        store.setAddress("北京 昌平区 金燕龙大厦");
//        store.setBrandId(brand.getBrandId());
//        storeService.save(store);

//        Store store2 =new Store();
//        store2.setStoreName("门店2");
//        store2.setProvince("北京");
//        store2.setCity("海淀");
//        store2.setArea("清华池");
//        store2.setAddress("北京 海淀 清华池");
//        store2.setBrandId(brand.getBrandId());
//        storeService.save(store2);
//
//        StoreManager storeManager =new StoreManager();
//        storeManager.setStoreManagerName("test");
//        storeManager.setStoreManagerPhone("18810973345");
//        storeManager.setPassword("123");
//        storeManagerService.save(storeManager);
//
//        //批量更新
//        store.setStoreManagerId(storeManager.getStoreManagerId());
//        store2.setStoreManagerId(storeManager.getStoreManagerId());
//        List<Store> stores = new ArrayList<>();
//        stores.add(store);
//        stores.add(store2);
//        storeService.updateBatchById(stores);
//}
}
