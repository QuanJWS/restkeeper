package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.shop.entity.Brand;
import com.restkeeper.tenant.TenantContext;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Executor;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BrandServiceImplTest extends BaseTest{
    @Reference(version = "1.0.0", check=false)
    private IBrandService brandService;


    @Test
    public void queryPage() {

        IPage<Brand> result = brandService.queryPage(Long.valueOf(1),Long.valueOf(100));
        result.getRecords().forEach(b->{
            System.out.println(b.getBrandName() + ":" + b.getInfo());
        });
//        for (int i = 0; i <10 ; i++) {
//            asyncServiceExecutor.execute(new Runnable() {
//                @Override
//                public void run() {
//                    IPage<Brand> result = brandService.queryPage(1,100);
//                    result.getRecords().forEach(b->{
//                        System.out.println(b.getBrandName() + ":" + b.getInfo());
//                    });
//                }
//            });
//        }
    }
}