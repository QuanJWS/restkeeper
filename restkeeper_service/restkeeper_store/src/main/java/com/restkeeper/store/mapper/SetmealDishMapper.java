package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.redis.MybatisRedisCache;
import com.restkeeper.store.entity.SetmealDish;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface SetmealDishMapper extends BaseMapper<SetmealDish>{
    /**
     * 根据套餐id获取菜品关联关系
     * @param setmealId
     * @return
     */
    @Select(value="select * from t_setmeal_dish where setmeal_id=#{setmealId} order by last_update_time desc")
    public List<SetmealDish> selectDishs(@Param("setmealId") String setmealId);


    /**
     * 根据菜品获取关联的套餐 (支持批量)
     * @param ids
     * @return
     */
   // @Select("select DISTINCT(setmeal_id) from t_setmeal_dish where dish_id=#{dishId}")
    @Select({
            "<script>",
            "select",
            "DISTINCT(setmeal_id)",
            "from t_setmeal_dish",
            "where dish_id in",
            "<foreach collection='ids' item='id' open='(' separator=',' close=')'>",
            "#{id}",
            "</foreach>",
            "</script>"
    })
    List<String> selectSetMealIdsByDishId(@Param("ids") List<String> ids);

    /**
     * 根据套餐获取关联的套餐
     * @return
     */
    @Select("select DISTINCT(dish_id) from t_setmeal_dish where setmeal_id=#{setmealId}")
    List<String> selectDishIdsBySetMealId(@Param("setmealId") String setmealId);

    /**
     * 根据套餐获取关联的套餐
     * @return
     */
    @Select("select dish_id from t_setmeal_dish where setmeal_id=#{setmealId}")
    List<String> selectDishsBySetMeals(@Param("setmealId") String setmealId);

}
