package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.store.entity.Remark;

/**
 * <p>
 * 备注信息 Mapper 接口
 * </p>
 */
public interface RemarkMapper extends BaseMapper<Remark> {

}
