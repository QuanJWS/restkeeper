package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beust.jcommander.internal.Lists;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.SetmealDish;
import com.restkeeper.store.mapper.SetmealDishMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Service(version = "1.0.0",protocol = "dubbo")
@Component
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements ISetmealDishService{
    @Autowired
    private IDishService dishService;
    @Override
    public boolean add(SetmealDish setmealDish) {
        return this.save(setmealDish);
    }


    @Override
    public List<String> selectDishsBySetMeal(String dishId) {
        return null;
    }

    @Override
    public List<Dish> getAllDishBySetmealId(String setmealId) {
        List<String> dishIdList = this.baseMapper.selectDishsBySetMeals(setmealId);
        if(dishIdList == null || dishIdList.size() <= 0){
            return new ArrayList<Dish>();
        }

        return dishService.listByIds(dishIdList);
    }

    @Override
    public Integer getDishCopiesInSetmeal(String dishId,String setmealId){
        QueryWrapper<SetmealDish> wrapper =  new QueryWrapper<>();
        wrapper.lambda()
                .eq(SetmealDish::getSetmealId,setmealId)
                .eq(SetmealDish::getDishId,dishId);
        SetmealDish setmealDish = this.getOne(wrapper);

        return setmealDish == null?0:setmealDish.getDishCopies();
    }

    @Override
    public List<String> selectSetMealIdsByDishId(List<String> ids) {
        return this.baseMapper.selectSetMealIdsByDishId(ids);
    }

    @Override
    public List<String> selectSetMealIdsByDishId(String dishId) {
        List<String> ids = Lists.newArrayList(dishId);
        return this.baseMapper.selectSetMealIdsByDishId(ids);
    }
}
