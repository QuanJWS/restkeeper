package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.store.entity.Table;
import com.restkeeper.store.entity.TableLog;
import com.restkeeper.store.mapper.TableLogMapper;
import com.restkeeper.utils.SystemCode;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service(version = "1.0.0",protocol = "dubbo")
@Component
public class TableLogServiceImpl extends ServiceImpl<TableLogMapper, TableLog> implements ITableLogService  {


    @Autowired
    private ITableService tableService;
    @Autowired
    private IDishService dishService;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean openTable(TableLog tableLog) {
        Table table= tableService.getById(tableLog.getTableId());
        //如果非空闲不能开桌
        if(SystemCode.TABLE_STATUS_FREE != table.getStatus()){
            throw  new BussinessException("桌台非空闲，不能开桌");
        }
        if(tableLog.getUserNumbers()>table.getSeatNumber()){
            throw  new BussinessException("就餐人数不能超过该桌台设置人数:"+table.getSeatNumber());
        }
        //开桌修改桌台状态
        table.setStatus(SystemCode.TABLE_STATUS_OPEND);
        tableService.updateById(table);
        //设置开桌时间
        tableLog.setCreateTime(LocalDateTime.now());
        tableLog.setTableStatus(SystemCode.TABLE_STATUS_OPEND);
        //保存开桌日志
        return this.save(tableLog);
    }

    /**
     * 根据桌台id获取最新开台日志
     * @param tableId
     * @return
     */
    @Override
    public TableLog getOpenTableLog(String tableId) {
        QueryWrapper<TableLog> tableLogQueryWrapper =new QueryWrapper<>();
        tableLogQueryWrapper.lambda().eq(TableLog::getTableId,tableId).orderByDesc(TableLog::getCreateTime);
        List<TableLog> logs= this.list(tableLogQueryWrapper);
        if(logs!=null&&logs.size()>=1){
            return logs.get(0);
        }
        return null;
    }



//    @Override
//    @GlobalTransactional
//    public boolean openTable(String tableId) {
//        Table table = tableService.getById(tableId);
//        table.setStatus(SystemCode.TABLE_STATUS_OPEND);
//
//        return tableService.updateById(table);

        //记录订单信息
//        OrderEntity orderEntity =new OrderEntity();
//        String orderId = IdWorker.getIdStr();
//        orderEntity.setOrderId(orderId);
//        orderEntity.setOrderNumber(IdWorker.getIdStr());
//        orderEntity.setTableId(opneTable.getTableId());
//        orderEntity.setTotalAmount(opneTable.getTotalAmount());
//        orderEntity.setCreateTime(LocalDateTime.now());
//        orderEntity.setRemark(opneTable.getRemarks());
//        //操作人员id
//        orderEntity.setOperatorName(TenantContext.getLoginUserName());
//        List<OrderDetailEntity> orderDetailEntityList =new ArrayList<>();
//
//        opneTable.getDishList().forEach(d->{
//            Dish dish = dishService.getById(d.getDishId());
//            OrderDetailEntity orderDetailEntity=new OrderDetailEntity();
//            orderDetailEntity.setDishId(d.getDishId());
//            orderDetailEntity.setTableId(orderEntity.getTableId());
//            orderDetailEntity.setOrderNumber(orderEntity.getOrderNumber());
//            orderDetailEntity.setDishType(SystemCode.DISH_TYPE_MORMAL);
//            orderDetailEntity.setDishName(dish.getDishName());
//            orderDetailEntity.setDishNumber(d.getDishNumber());
//            orderDetailEntity.setDishPrice(dish.getPrice());
//            orderDetailEntity.setDishRemark(d.getDishRemark());
//            orderDetailEntityList.add(orderDetailEntity);
//        });
//
//        orderService.save(orderEntity);
//        //记录开桌日志
//        TableLog tablelog =new TableLog();
//        tablelog.setOrderId(orderId);
//        tablelog.setTableId(opneTable.getTableId());
//        tablelog.setPersonNumbers(opneTable.getPersonNumbers());
//        tablelog.setUserId(TenantContext.getLoginUserId());
//        tablelog.setTableStatus(table.getStatus());
//        this.save(tablelog);
//
//        return true;
//    }
}
