package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.redis.MybatisRedisCache;
import com.restkeeper.store.entity.Setmeal;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface SetmealMapper extends BaseMapper<Setmeal>{

    /**
     * 根据分类获取套餐列表
     * @param categoryId
     * @return
     */
    @Select(value="select * from t_setmeal where category_id=#{categoryId} and is_deleted=0 order by last_update_time desc")
    public List<Setmeal> selectSetmealByCategory(@Param("categoryId") String categoryId);
}
