package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.store.entity.Staff;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.bouncycastle.util.Store;

import java.util.List;

/**
 * <p>
 * 员工信息 Mapper 接口
 * </p>
 */
public interface StaffMapper extends BaseMapper<Staff> {

    @Select(value="select * from t_staff where phone=#{loginname} and shop_id=#{shopId}")
    public Staff selectStoreByManager(@Param("loginname") String loginname,@Param("shopId") String shopId);
}
