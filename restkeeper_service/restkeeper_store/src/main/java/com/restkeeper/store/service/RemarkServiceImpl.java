package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.entity.Dictionary;
import com.restkeeper.service.ISysDictService;
import com.restkeeper.store.entity.Remark;
import com.restkeeper.store.entity.TableArea;
import com.restkeeper.store.mapper.RemarkMapper;
import com.restkeeper.utils.SystemCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 备注信息 服务实现类
 */
@Service(version = "1.0.0",protocol = "dubbo")
public class RemarkServiceImpl extends ServiceImpl<RemarkMapper, Remark> implements IRemarkService {

    @Reference(version = "1.0.0", check=false)
    private ISysDictService sysDictService;

    @Override
    public List<Remark> getRemarks() {
        List<Remark> remarks= this.list();
        //如果门店没有设置过备注，直接返回系统默认设置备注信息
        if(remarks==null||remarks.isEmpty()){
            remarks =new ArrayList<>();
            List<Dictionary>  list= sysDictService.getDictionaryList(SystemCode.DICTIONARY_REMARK);
            for (Dictionary dictionary : list) {
                Remark remark =new Remark();
                remark.setRemarkName(dictionary.getDictName());
                remark.setRemarkValue(dictionary.getDictData());
                remarks.add(remark);
            }
        }
        return remarks;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean updateRemarks(List<Remark> remarks) {
        //物理删除以前门店
        QueryWrapper<Remark> wrapper = new QueryWrapper<>();
        this.remove(wrapper);
        //批量插入新的数据
        return this.saveBatch(remarks);
    }

    @Override
    public List<String> getRemark(String remarkName) {
        QueryWrapper<Remark> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Remark::getRemarkName,remarkName);
        Remark remark = this.getOne(wrapper);
        if(remark!=null){
            String remarkValue = remark.getRemarkValue();
            String remarkValue_substring=remarkValue.substring(remarkValue.indexOf("[")+1,remarkValue.indexOf("]"));
            if(StringUtils.isNotEmpty(remarkValue_substring)){
                String[] remark_array= remarkValue_substring.split(",");
                return Arrays.asList(remark_array);
            }
        }
        return new ArrayList<>();
    }
}
