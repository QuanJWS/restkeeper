package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.entity.SetmealDish;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.RpcContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SetmealDishServiceImplTest extends  BaseTest{

//    @Reference(version = "1.0.0", check=false)
//    private IDishService dishService;

    @Reference(version = "1.0.0", check=false)
    private IDishService dishService;

    @Reference(version = "1.0.0", check=false)
    private ISetmealService setmealService;

    @Test
    public void add() {


        Setmeal setmeal= new Setmeal();
        setmeal.setCategoryId("1");
        setmeal.setName("宫保鸡丁套餐");
        setmeal.setCode("gbjd");
        setmeal.setDescription("好吃");

//        List<SetmealDish> setmealDishes =new ArrayList<SetmealDish>();
////        for (SetmealDish setmealDish : setmealDishes) {
////            setmealDish.setDishId(1);
////        }
////        setmealService.add()
//        IPage<Setmeal> result =setmealService.queryPage(1L,1L);
//        Assert.assertTrue(true);
    }
    @Test
    public void queryPage() {
        IPage<Setmeal> result =setmealService.queryPage(1L,1L);
        Assert.assertTrue(true);
    }
}