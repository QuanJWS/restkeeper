package com.restkeeper.store.service;

import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.Setmeal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SetmealTest extends  BaseTest {
    @Autowired
    private ISetmealService setmealService;

    @Autowired
    private ISetmealDishService setmealDishService;
    @Test
    public void add(){
        Setmeal result = setmealService.getById("6352890890");
        System.out.println(result);
    }

    @Test
    public void getDishIdsBySetmeal(){
        List<Dish> result = setmealDishService.getAllDishBySetmealId("6352890890");

        System.out.println(result);
    }
}
