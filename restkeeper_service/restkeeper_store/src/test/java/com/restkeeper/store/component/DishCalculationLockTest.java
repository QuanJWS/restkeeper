package com.restkeeper.store.component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.restkeeper.lock.CalculationBusinessLock;
import com.restkeeper.store.entity.SellCalculation;
import com.restkeeper.store.service.BaseTest;
import com.restkeeper.store.service.ISellCalculationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DishCalculationLockTest extends BaseTest{
    @Autowired
    private CalculationBusinessLock lock;
    @Autowired
    private ISellCalculationService sellCalculationService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testLock() {
        String dishId = "2";
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                init();
                boolean lockSuccess = lock.spinLock(dishId, () -> sellCalculationService.getRemainderCount(dishId));
                if (lockSuccess) {
                    System.out.println("--------" + Thread.currentThread().getName() + " 获取锁lock成功");
                    QueryWrapper<SellCalculation> qw = new QueryWrapper<>();
                    qw.lambda().eq(SellCalculation::getDishId, dishId);
                    SellCalculation sellCalculation = sellCalculationService.getOne(qw);
                    sellCalculation.setRemainder(sellCalculation.getRemainder() - 1);
                    sellCalculationService.updateById(sellCalculation);
                    lock.unlock(dishId);
                } else {
                    System.out.println("--------" + Thread.currentThread().getName() + " 没有获取到锁，修改菜失败");
                }
            }).start();
        }
        try {
            TimeUnit.SECONDS.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
