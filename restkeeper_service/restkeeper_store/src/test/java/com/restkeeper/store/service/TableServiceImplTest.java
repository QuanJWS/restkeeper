package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.store.entity.Table;
import org.apache.dubbo.rpc.RpcContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TableServiceImplTest extends  BaseTest{
    @Autowired
    private ITableService tableService;

    @Test
    public void searchByArea() {
        IPage<Table> result = tableService.queryPageByArea("34234234",1,10);
        Assert.assertTrue(true);
    }

    @Test
    public void getTable() {
        Table table= tableService.getById("46252252");
        System.out.println(table);
    }
}