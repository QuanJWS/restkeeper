package com.restkeeper.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class PassWordUtils {

    /**
     * 密码随机数
     * @return
     */
    public static String getRandom(int weight) {
        return RandomStringUtils.random(weight,
                new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 's',
                        't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9' });
    }
}
