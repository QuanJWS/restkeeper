package com.restkeeper.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class Calculator{
    /**
     * 获取百分比
     * @param source 被除数
     * @param divisor 除数
     * @return
     */
    public static BigDecimal getRate(Integer source,Integer divisor){
        BigDecimal sourceNumber = new BigDecimal(source);
        BigDecimal divideNumber = new BigDecimal(divisor);

        return sourceNumber.divide(divideNumber,4,BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 按比率获取最终结果
     * @param source
     * @param rate
     * @return
     */
    public static Integer getPercentResult(Integer source,BigDecimal rate){
        BigDecimal sourceNumber = new BigDecimal(source);
        sourceNumber = sourceNumber.multiply(rate.setScale(1,BigDecimal.ROUND_HALF_UP));
        NumberFormat integer = NumberFormat.getIntegerInstance();

        return Integer.valueOf(integer.format(sourceNumber.doubleValue()));
    }
}
