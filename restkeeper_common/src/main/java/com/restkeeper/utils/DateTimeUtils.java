package com.restkeeper.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {

   private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

   public static String format(LocalDateTime localDateTime){
       return dateTimeFormatter.format(localDateTime);
   }

    public static void main(String[] args) {
        System.out.println(DateTimeUtils.format(LocalDateTime.now()));
    }
}
