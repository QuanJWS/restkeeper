/*
SQLyog Ultimate v12.3.1 (64 bit)
MySQL - 8.0.18 : Database - restkeeper_order
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`restkeeper_order` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `restkeeper_order`;

/*Table structure for table `t_his_order` */

DROP TABLE IF EXISTS `t_his_order`;

CREATE TABLE `t_his_order` (
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `order_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '流水号',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属商户',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属门店',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '桌台id',
  `pay_status` int(11) DEFAULT '0' COMMENT '支付状态 0 未付 1 已付  ',
  `pay_type` int(11) DEFAULT NULL COMMENT '付款方式 0 免单 1 现金 2 微信 3 支付宝  4 银行卡 5挂账',
  `total_amount` int(11) DEFAULT NULL COMMENT '应收金额',
  `small_amount` int(11) DEFAULT NULL COMMENT '抹零金额',
  `present_amount` int(11) DEFAULT NULL COMMENT '赠菜金额',
  `pay_amount` int(11) DEFAULT NULL COMMENT '付款金额',
  `free_amount` int(11) DEFAULT NULL COMMENT '免单金额',
  `person_numbers` int(11) DEFAULT NULL COMMENT '就餐人数',
  `order_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '整单备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_update_time` datetime NOT NULL COMMENT '最后更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `operator_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作人名称',
  `buy_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '够买人id',
  `order_source` int(11) NOT NULL DEFAULT '0' COMMENT '订单来源 0=堂点 1=小程序',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='历史订单主表';

/*Data for the table `t_his_order` */

insert  into `t_his_order`(`order_id`,`order_number`,`shop_id`,`store_id`,`table_id`,`pay_status`,`pay_type`,`total_amount`,`small_amount`,`present_amount`,`pay_amount`,`free_amount`,`person_numbers`,`order_remark`,`create_time`,`last_update_time`,`is_deleted`,`operator_name`,`buy_id`,`order_source`) values 
('1231947349933264898','202002240008','test','1206477268886712321','1227828071432716291',0,NULL,3000,NULL,NULL,NULL,NULL,3,NULL,'2020-02-27 22:21:51','2020-03-04 22:21:51',0,'张三',NULL,0),
('1231947518896607234','202002240009','test','1206477268886712321','1227828071432716292',1,1,20000,1,2,20000,3,1,NULL,'2020-02-27 22:22:32','2020-03-04 11:27:26',0,'张三',NULL,0),
('1231947620361015298','202002240010','test','1206477268886712321','1227828071432716293',0,NULL,10000,NULL,NULL,NULL,NULL,1,NULL,'2020-02-27 22:22:56','2020-03-04 15:22:56',0,'张三',NULL,0),
('1231947739722518529','202002240011','test','1206477268886712321','1227828071432716294',0,NULL,6000,NULL,NULL,NULL,NULL,1,NULL,'2020-02-27 22:23:24','2020-03-04 22:23:24',0,'张三',NULL,0),
('1232944332982493186','202002270014','test','1206477268886712321','1227828071432716295',1,3,8000,500,5,8000,6,1,NULL,'2020-02-27 16:23:31','2020-03-04 16:23:41',0,'张三',NULL,0);

/*Table structure for table `t_his_order_detail` */

DROP TABLE IF EXISTS `t_his_order_detail`;

CREATE TABLE `t_his_order_detail` (
  `detail_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单id',
  `order_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单流水号',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属商户',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属门店',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属桌台',
  `detail_status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1正常 2 赠菜 3 退菜  4 加菜',
  `flavor_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '口味备注',
  `present_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '赠菜备注',
  `return_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '退菜备注',
  `add_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '加菜备注',
  `dish_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜品id',
  `dish_type` int(11) NOT NULL COMMENT '类型 1 菜品 2 套餐',
  `dish_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `dish_price` int(11) NOT NULL COMMENT '单价',
  `dish_number` int(11) NOT NULL COMMENT '份数',
  `dish_amount` int(11) DEFAULT NULL COMMENT '金额',
  `dish_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注信息',
  `dish_category_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜品分类名称',
  `last_update_time` datetime NOT NULL COMMENT '最后更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`detail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='历史订单详情表';

/*Data for the table `t_his_order_detail` */

insert  into `t_his_order_detail`(`detail_id`,`order_id`,`order_number`,`shop_id`,`store_id`,`table_id`,`detail_status`,`flavor_remark`,`present_remark`,`return_remark`,`add_remark`,`dish_id`,`dish_type`,`dish_name`,`dish_price`,`dish_number`,`dish_amount`,`dish_remark`,`dish_category_name`,`last_update_time`,`is_deleted`) values 
('1231947350117814273','1231947349933264898','2020022400080001','test','1206477268886712321','1227828071432716291',1,'[辣]',NULL,NULL,NULL,'1215921443154501633',1,'麻婆豆腐',3000,1,3000,NULL,'素菜','2020-03-04 22:21:51',0),
('1231947519072768001','1231947518896607234','2020022400090001','test','1206477268886712321','1227828071432716292',1,'[辣]',NULL,NULL,NULL,'1215921732100104194',1,'烤生蚝',4000,5,200000,NULL,'荤菜','2020-03-04 22:22:32',0),
('1231947620558147585','1231947620361015298','2020022400100001','test','1206477268886712321','1227828071432716293',1,'[辣]',NULL,NULL,NULL,'1215921732100104195',1,'炸酥肉',5000,2,10000,NULL,'川菜','2020-03-04 22:22:56',0),
('1231947739944816642','1231947739722518529','2020022400110001','test','1206477268886712321','1227828071432716294',1,'[无]',NULL,NULL,NULL,'1215921732100104196',1,'家常凉菜',6000,1,6000,NULL,'凉菜','2020-03-04 22:23:24',0),
('1232944374929727490','1232944332982493186','2020022700140001','test','1206477268886712321','1227828071432716295',1,'[无]',NULL,NULL,NULL,'1215921443154501633',1,'麻婆豆腐',3000,1,2182,NULL,'素菜','2020-03-04 16:23:41',0),
('1232944374984253442','1232944332982493186','2020022700140001','test','1206477268886712321','1227828071432716295',1,'[无]',NULL,NULL,NULL,'1215921732100104194',1,'烤生蚝',4000,2,5818,NULL,'荤菜','2020-03-04 16:23:41',0);

/*Table structure for table `t_order` */

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `order_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '流水号',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属商户',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属门店',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '桌台id',
  `pay_status` int(11) DEFAULT '0' COMMENT '支付状态 0 未付 1 已付  2 支付中',
  `pay_type` int(11) DEFAULT NULL COMMENT '付款方式 0 免单 1 现金 2 微信 3 支付宝  4 银行卡 5挂账',
  `total_amount` int(11) DEFAULT NULL COMMENT '应收金额',
  `small_amount` int(11) DEFAULT NULL COMMENT '抹零金额',
  `present_amount` int(11) DEFAULT NULL COMMENT '璧犺彍閲戦',
  `pay_amount` int(11) DEFAULT NULL COMMENT '付款金额',
  `free_amount` int(11) DEFAULT NULL COMMENT '免单金额',
  `person_numbers` int(11) DEFAULT NULL COMMENT '就餐人数',
  `order_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '鏁村崟澶囨敞',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_update_time` datetime NOT NULL COMMENT '最后更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `operator_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '操作人名称',
  `buy_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '够买人id',
  `order_source` int(11) NOT NULL DEFAULT '0' COMMENT '订单来源 0=堂点 1=小程序',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='订单主表';

/*Data for the table `t_order` */

/*Table structure for table `t_order_detail` */

DROP TABLE IF EXISTS `t_order_detail`;

CREATE TABLE `t_order_detail` (
  `detail_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单id',
  `order_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单流水号',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属商户',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属门店',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属桌台',
  `detail_status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1正常 2 赠菜 3 退菜  4 加菜',
  `flavor_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '口味备注',
  `present_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '赠菜备注',
  `return_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '閫€鑿滃娉?',
  `add_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '加菜备注',
  `dish_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜品id',
  `dish_type` int(11) NOT NULL COMMENT '类型 1 菜品 2 套餐',
  `dish_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `dish_price` int(11) NOT NULL COMMENT '单价',
  `dish_number` int(11) NOT NULL COMMENT '份数',
  `dish_amount` int(11) DEFAULT NULL COMMENT '金额',
  `dish_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注信息',
  `dish_category_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜品分类名称',
  `last_update_time` datetime NOT NULL COMMENT '最后更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`detail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='订单详情表';

/*Data for the table `t_order_detail` */

insert  into `t_order_detail`(`detail_id`,`order_id`,`order_number`,`shop_id`,`store_id`,`table_id`,`detail_status`,`flavor_remark`,`present_remark`,`return_remark`,`add_remark`,`dish_id`,`dish_type`,`dish_name`,`dish_price`,`dish_number`,`dish_amount`,`dish_remark`,`dish_category_name`,`last_update_time`,`is_deleted`) values 
('1232944333976543234','1232944332982493186','2020022700140001','test','1206477268886712321','1227828071432716295',1,'[无]',NULL,NULL,NULL,'1216262319877738497',2,'经济套餐',8000,1,8000,NULL,'经济套餐','2020-03-04 16:23:31',0);

/*Table structure for table `t_order_detail_meal` */

DROP TABLE IF EXISTS `t_order_detail_meal`;

CREATE TABLE `t_order_detail_meal` (
  `detail_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单id',
  `order_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单流水号',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属商户',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属门店',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属桌台',
  `detail_status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1正常 2 赠菜 3 退菜  4 加菜',
  `flavor_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '口味备注',
  `present_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '赠菜备注',
  `return_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '退菜备注',
  `add_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '加菜备注',
  `dish_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜品id',
  `dish_type` int(11) NOT NULL COMMENT '类型 1 菜品 2 套餐',
  `dish_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `dish_price` int(11) NOT NULL COMMENT '单价',
  `dish_number` int(11) NOT NULL COMMENT '份数',
  `dish_amount` int(11) DEFAULT NULL COMMENT '金额',
  `dish_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注信息',
  `dish_category_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜品分类名称',
  `last_update_time` datetime NOT NULL COMMENT '最后更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`detail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='套餐订单详情表';

/*Data for the table `t_order_detail_meal` */

/*Table structure for table `t_report_dish` */

DROP TABLE IF EXISTS `t_report_dish`;

CREATE TABLE `t_report_dish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pay_date` date DEFAULT NULL COMMENT '支付日期',
  `category` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '分类名称',
  `dish_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜品名称',
  `dish_number` int(11) DEFAULT NULL COMMENT '销售量',
  `dish_money` int(11) DEFAULT NULL COMMENT '销售额',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '集团id',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '店铺id',
  `last_update_time` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1235392805904379907 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_report_dish` */

insert  into `t_report_dish`(`id`,`pay_date`,`category`,`dish_name`,`dish_number`,`dish_money`,`shop_id`,`store_id`,`last_update_time`,`is_deleted`) values 
(1235392805669498881,'2020-03-04','素菜','麻婆豆腐',2,5182,'test','1206477268886712321','2020-03-05 10:32:52',0),
(1235392805749190658,'2020-03-04','荤菜','烤生蚝',7,205818,'test','1206477268886712321','2020-03-05 10:32:52',0),
(1235392805875019778,'2020-03-04','川菜','炸酥肉',2,10000,'test','1206477268886712321','2020-03-05 10:32:52',0),
(1235392805904379906,'2020-03-04','凉菜','家常凉菜',1,6000,'test','1206477268886712321','2020-03-05 10:32:52',0);

/*Table structure for table `t_report_pay` */

DROP TABLE IF EXISTS `t_report_pay`;

CREATE TABLE `t_report_pay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pay_date` date DEFAULT NULL COMMENT '日期',
  `pay_type` int(11) DEFAULT NULL COMMENT '收款方式',
  `total_amount` int(11) DEFAULT NULL COMMENT '应收金额',
  `present_amount` int(11) DEFAULT NULL COMMENT '赠送金额',
  `small_amount` int(11) DEFAULT NULL COMMENT '抹零金额',
  `free_amount` int(11) DEFAULT '0' COMMENT '免单金额',
  `pay_amount` int(11) DEFAULT NULL COMMENT '实收金额',
  `person_numbers` int(11) DEFAULT NULL COMMENT '就餐人数',
  `pay_count` int(11) DEFAULT '0' COMMENT '交易单数',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '集团ID',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '店铺ID',
  `last_update_time` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1235392805560446978 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_report_pay` */

insert  into `t_report_pay`(`id`,`pay_date`,`pay_type`,`total_amount`,`present_amount`,`small_amount`,`free_amount`,`pay_amount`,`person_numbers`,`pay_count`,`shop_id`,`store_id`,`last_update_time`,`is_deleted`) values 
(1235392805493338114,'2020-03-05',NULL,19000,NULL,NULL,0,NULL,5,0,'test','1206477268886712321','2020-03-05 10:32:52',0),
(1235392805539475457,'2020-03-05',1,20000,2,1,3,20000,1,1,'test','1206477268886712321','2020-03-05 10:32:52',0),
(1235392805560446977,'2020-03-05',3,8000,5,500,6,8000,1,1,'test','1206477268886712321','2020-03-05 10:32:52',0);

/*Table structure for table `t_report_time` */

DROP TABLE IF EXISTS `t_report_time`;

CREATE TABLE `t_report_time` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pay_date` date DEFAULT NULL COMMENT '日期',
  `pay_time` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '时间段',
  `table_count` int(11) DEFAULT NULL COMMENT '单数',
  `tatal_amount` int(11) DEFAULT NULL COMMENT '销售额',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '集团id',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_report_time` */

/*Table structure for table `t_reverse_order` */

DROP TABLE IF EXISTS `t_reverse_order`;

CREATE TABLE `t_reverse_order` (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '关联订单号',
  `order_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '流水号',
  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属商户',
  `store_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属门店',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '桌台id',
  `status` int(11) DEFAULT '1' COMMENT ' 0 失败 1 成功  ',
  `amount` int(11) DEFAULT NULL COMMENT '反结账金额',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_update_time` datetime NOT NULL COMMENT '最后更新时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `operator_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '操作人名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='反结账主表';

/*Data for the table `t_reverse_order` */

/*Table structure for table `undo_log` */

DROP TABLE IF EXISTS `undo_log`;

CREATE TABLE `undo_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) NOT NULL,
  `xid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `context` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(11) NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  `ext` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `undo_log` */

/*Table structure for table `v_order` */

DROP TABLE IF EXISTS `v_order`;

/*!50001 DROP VIEW IF EXISTS `v_order` */;
/*!50001 DROP TABLE IF EXISTS `v_order` */;

/*!50001 CREATE TABLE  `v_order`(
 `order_id` varchar(32) ,
 `order_number` varchar(32) ,
 `shop_id` varchar(32) ,
 `store_id` varchar(32) ,
 `table_id` varchar(32) ,
 `pay_status` int(11) ,
 `pay_type` int(11) ,
 `total_amount` int(11) ,
 `small_amount` int(11) ,
 `present_amount` int(11) ,
 `pay_amount` int(11) ,
 `free_amount` int(11) ,
 `person_numbers` int(11) ,
 `order_remark` varchar(255) ,
 `create_time` datetime ,
 `last_update_time` datetime ,
 `is_deleted` int(11) ,
 `operator_name` varchar(32) ,
 `buy_id` varchar(32) ,
 `order_source` int(11) 
)*/;

/*Table structure for table `v_order_detail` */

DROP TABLE IF EXISTS `v_order_detail`;

/*!50001 DROP VIEW IF EXISTS `v_order_detail` */;
/*!50001 DROP TABLE IF EXISTS `v_order_detail` */;

/*!50001 CREATE TABLE  `v_order_detail`(
 `detail_id` varchar(32) ,
 `order_id` varchar(32) ,
 `order_number` varchar(32) ,
 `shop_id` varchar(32) ,
 `store_id` varchar(32) ,
 `table_id` varchar(32) ,
 `detail_status` int(11) ,
 `flavor_remark` varchar(255) ,
 `present_remark` varchar(255) ,
 `return_remark` varchar(255) ,
 `add_remark` varchar(255) ,
 `dish_id` varchar(32) ,
 `dish_type` int(11) ,
 `dish_name` varchar(255) ,
 `dish_price` int(11) ,
 `dish_number` int(11) ,
 `dish_amount` int(11) ,
 `dish_remark` varchar(255) ,
 `dish_category_name` varchar(32) ,
 `last_update_time` datetime ,
 `is_deleted` int(11) 
)*/;

/*Table structure for table `v_order_detail_all` */

DROP TABLE IF EXISTS `v_order_detail_all`;

/*!50001 DROP VIEW IF EXISTS `v_order_detail_all` */;
/*!50001 DROP TABLE IF EXISTS `v_order_detail_all` */;

/*!50001 CREATE TABLE  `v_order_detail_all`(
 `detail_id` varchar(32) ,
 `order_id` varchar(32) ,
 `order_number` varchar(32) ,
 `shop_id` varchar(32) ,
 `store_id` varchar(32) ,
 `table_id` varchar(32) ,
 `detail_status` int(11) ,
 `flavor_remark` varchar(255) ,
 `present_remark` varchar(255) ,
 `return_remark` varchar(255) ,
 `add_remark` varchar(255) ,
 `dish_id` varchar(32) ,
 `dish_type` int(11) ,
 `dish_name` varchar(255) ,
 `dish_price` int(11) ,
 `dish_number` int(11) ,
 `dish_amount` int(11) ,
 `dish_remark` varchar(255) ,
 `dish_category_name` varchar(32) ,
 `last_update_time` datetime ,
 `is_deleted` int(11) 
)*/;

/*View structure for view v_order */

/*!50001 DROP TABLE IF EXISTS `v_order` */;
/*!50001 DROP VIEW IF EXISTS `v_order` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_order` AS select `t_order`.`order_id` AS `order_id`,`t_order`.`order_number` AS `order_number`,`t_order`.`shop_id` AS `shop_id`,`t_order`.`store_id` AS `store_id`,`t_order`.`table_id` AS `table_id`,`t_order`.`pay_status` AS `pay_status`,`t_order`.`pay_type` AS `pay_type`,`t_order`.`total_amount` AS `total_amount`,`t_order`.`small_amount` AS `small_amount`,`t_order`.`present_amount` AS `present_amount`,`t_order`.`pay_amount` AS `pay_amount`,`t_order`.`free_amount` AS `free_amount`,`t_order`.`person_numbers` AS `person_numbers`,`t_order`.`order_remark` AS `order_remark`,`t_order`.`create_time` AS `create_time`,`t_order`.`last_update_time` AS `last_update_time`,`t_order`.`is_deleted` AS `is_deleted`,`t_order`.`operator_name` AS `operator_name`,`t_order`.`buy_id` AS `buy_id`,`t_order`.`order_source` AS `order_source` from `t_order` union all select `t_his_order`.`order_id` AS `order_id`,`t_his_order`.`order_number` AS `order_number`,`t_his_order`.`shop_id` AS `shop_id`,`t_his_order`.`store_id` AS `store_id`,`t_his_order`.`table_id` AS `table_id`,`t_his_order`.`pay_status` AS `pay_status`,`t_his_order`.`pay_type` AS `pay_type`,`t_his_order`.`total_amount` AS `total_amount`,`t_his_order`.`small_amount` AS `small_amount`,`t_his_order`.`present_amount` AS `present_amount`,`t_his_order`.`pay_amount` AS `pay_amount`,`t_his_order`.`free_amount` AS `free_amount`,`t_his_order`.`person_numbers` AS `person_numbers`,`t_his_order`.`order_remark` AS `order_remark`,`t_his_order`.`create_time` AS `create_time`,`t_his_order`.`last_update_time` AS `last_update_time`,`t_his_order`.`is_deleted` AS `is_deleted`,`t_his_order`.`operator_name` AS `operator_name`,`t_his_order`.`buy_id` AS `buy_id`,`t_his_order`.`order_source` AS `order_source` from `t_his_order` */;

/*View structure for view v_order_detail */

/*!50001 DROP TABLE IF EXISTS `v_order_detail` */;
/*!50001 DROP VIEW IF EXISTS `v_order_detail` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_order_detail` AS select `t_order_detail`.`detail_id` AS `detail_id`,`t_order_detail`.`order_id` AS `order_id`,`t_order_detail`.`order_number` AS `order_number`,`t_order_detail`.`shop_id` AS `shop_id`,`t_order_detail`.`store_id` AS `store_id`,`t_order_detail`.`table_id` AS `table_id`,`t_order_detail`.`detail_status` AS `detail_status`,`t_order_detail`.`flavor_remark` AS `flavor_remark`,`t_order_detail`.`present_remark` AS `present_remark`,`t_order_detail`.`return_remark` AS `return_remark`,`t_order_detail`.`add_remark` AS `add_remark`,`t_order_detail`.`dish_id` AS `dish_id`,`t_order_detail`.`dish_type` AS `dish_type`,`t_order_detail`.`dish_name` AS `dish_name`,`t_order_detail`.`dish_price` AS `dish_price`,`t_order_detail`.`dish_number` AS `dish_number`,`t_order_detail`.`dish_amount` AS `dish_amount`,`t_order_detail`.`dish_remark` AS `dish_remark`,`t_order_detail`.`dish_category_name` AS `dish_category_name`,`t_order_detail`.`last_update_time` AS `last_update_time`,`t_order_detail`.`is_deleted` AS `is_deleted` from `t_order_detail` union all select `t_his_order_detail`.`detail_id` AS `detail_id`,`t_his_order_detail`.`order_id` AS `order_id`,`t_his_order_detail`.`order_number` AS `order_number`,`t_his_order_detail`.`shop_id` AS `shop_id`,`t_his_order_detail`.`store_id` AS `store_id`,`t_his_order_detail`.`table_id` AS `table_id`,`t_his_order_detail`.`detail_status` AS `detail_status`,`t_his_order_detail`.`flavor_remark` AS `flavor_remark`,`t_his_order_detail`.`present_remark` AS `present_remark`,`t_his_order_detail`.`return_remark` AS `return_remark`,`t_his_order_detail`.`add_remark` AS `add_remark`,`t_his_order_detail`.`dish_id` AS `dish_id`,`t_his_order_detail`.`dish_type` AS `dish_type`,`t_his_order_detail`.`dish_name` AS `dish_name`,`t_his_order_detail`.`dish_price` AS `dish_price`,`t_his_order_detail`.`dish_number` AS `dish_number`,`t_his_order_detail`.`dish_amount` AS `dish_amount`,`t_his_order_detail`.`dish_remark` AS `dish_remark`,`t_his_order_detail`.`dish_category_name` AS `dish_category_name`,`t_his_order_detail`.`last_update_time` AS `last_update_time`,`t_his_order_detail`.`is_deleted` AS `is_deleted` from `t_his_order_detail` */;

/*View structure for view v_order_detail_all */

/*!50001 DROP TABLE IF EXISTS `v_order_detail_all` */;
/*!50001 DROP VIEW IF EXISTS `v_order_detail_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_order_detail_all` AS select `t_order_detail`.`detail_id` AS `detail_id`,`t_order_detail`.`order_id` AS `order_id`,`t_order_detail`.`order_number` AS `order_number`,`t_order_detail`.`shop_id` AS `shop_id`,`t_order_detail`.`store_id` AS `store_id`,`t_order_detail`.`table_id` AS `table_id`,`t_order_detail`.`detail_status` AS `detail_status`,`t_order_detail`.`flavor_remark` AS `flavor_remark`,`t_order_detail`.`present_remark` AS `present_remark`,`t_order_detail`.`return_remark` AS `return_remark`,`t_order_detail`.`add_remark` AS `add_remark`,`t_order_detail`.`dish_id` AS `dish_id`,`t_order_detail`.`dish_type` AS `dish_type`,`t_order_detail`.`dish_name` AS `dish_name`,`t_order_detail`.`dish_price` AS `dish_price`,`t_order_detail`.`dish_number` AS `dish_number`,`t_order_detail`.`dish_amount` AS `dish_amount`,`t_order_detail`.`dish_remark` AS `dish_remark`,`t_order_detail`.`dish_category_name` AS `dish_category_name`,`t_order_detail`.`last_update_time` AS `last_update_time`,`t_order_detail`.`is_deleted` AS `is_deleted` from `t_order_detail` where (`t_order_detail`.`dish_type` = 1) union all select `t_order_detail_meal`.`detail_id` AS `detail_id`,`t_order_detail_meal`.`order_id` AS `order_id`,`t_order_detail_meal`.`order_number` AS `order_number`,`t_order_detail_meal`.`shop_id` AS `shop_id`,`t_order_detail_meal`.`store_id` AS `store_id`,`t_order_detail_meal`.`table_id` AS `table_id`,`t_order_detail_meal`.`detail_status` AS `detail_status`,`t_order_detail_meal`.`flavor_remark` AS `flavor_remark`,`t_order_detail_meal`.`present_remark` AS `present_remark`,`t_order_detail_meal`.`return_remark` AS `return_remark`,`t_order_detail_meal`.`add_remark` AS `add_remark`,`t_order_detail_meal`.`dish_id` AS `dish_id`,`t_order_detail_meal`.`dish_type` AS `dish_type`,`t_order_detail_meal`.`dish_name` AS `dish_name`,`t_order_detail_meal`.`dish_price` AS `dish_price`,`t_order_detail_meal`.`dish_number` AS `dish_number`,`t_order_detail_meal`.`dish_amount` AS `dish_amount`,`t_order_detail_meal`.`dish_remark` AS `dish_remark`,`t_order_detail_meal`.`dish_category_name` AS `dish_category_name`,`t_order_detail_meal`.`last_update_time` AS `last_update_time`,`t_order_detail_meal`.`is_deleted` AS `is_deleted` from `t_order_detail_meal` where (`t_order_detail_meal`.`dish_type` = 1) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
