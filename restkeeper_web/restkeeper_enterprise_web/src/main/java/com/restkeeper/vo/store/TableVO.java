package com.restkeeper.vo.store;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class TableVO implements Serializable{
    @ApiModelProperty(value = "桌台名称")
    private String name;
    @ApiModelProperty(value = "座位数")
    private int seatNumber;
    @ApiModelProperty(value = "区域id")
    private String areaId;
}
