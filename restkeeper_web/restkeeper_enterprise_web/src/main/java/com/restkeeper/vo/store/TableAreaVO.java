package com.restkeeper.vo.store;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class TableAreaVO implements Serializable{
    @ApiModelProperty(value = "区域id")
    private String id;
    @ApiModelProperty(value = "区域名称")
    private String name;
}
