package com.restkeeper.vo.shop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateBrandVO extends AddTBrandVO {

	@ApiModelProperty(value = "商户id")
	private String brandId;

}
