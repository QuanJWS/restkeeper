package com.restkeeper.controller.store;

import com.google.common.collect.Lists;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.response.page.PageVO;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.entity.SetmealDish;
import com.restkeeper.store.service.IDishService;
import com.restkeeper.store.service.ISetmealService;
import com.restkeeper.utils.SystemCode;
import com.restkeeper.vo.store.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = { "套餐管理" })
@RestController
@RequestMapping("/setMeal")
public class SetmealController{
    @Reference(version = "1.0.0", check=false)
    private ISetmealService setmealService;

    @Reference(version = "1.0.0", check=false)
    private IDishService dishService;



    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取套餐信息")
    public SetmealVO getDish(@PathVariable String id){
        Setmeal setmeal = setmealService.getById(id);
        if(setmeal==null){
            throw new BussinessException("套餐不存在");
        }
        SetmealVO setmealVo=new SetmealVO();
        BeanUtils.copyProperties(setmeal, setmealVo);
        //口味列表
        List<SetmealDish> setmealDishList = setmeal.getDishList();
        List<SetmealDishVO> setmealDishVOList=new ArrayList<>();
        for (SetmealDish setmealDish : setmealDishList) {
            Dish dish= dishService.getById(setmealDish.getDishId());
            SetmealDishVO setmealDishVO =new SetmealDishVO();
            setmealDishVO.setDishId(setmealDish.getDishId());
            setmealDishVO.setDishName(dish.getName());
            setmealDishVO.setCopies(setmealDish.getDishCopies());
            setmealDishVO.setPrice(dish.getPrice());
            setmealDishVOList.add(setmealDishVO);
        }

        setmealVo.setDishList(setmealDishVOList);
        return  setmealVo;
    }
    /**
     * 添加套餐
     * @param setmealVO
     */
    @ApiOperation(value = "添加套餐")
    @PostMapping("/add")
    public boolean add(@RequestBody SetmealVO setmealVO)  {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealVO,setmeal);
        List<SetmealDish> setmealDishList = Lists.newArrayList();
        if(setmealVO.getDishList() != null){
            setmealVO.getDishList().forEach(d->{
                SetmealDish setmealDish = new SetmealDish();
                setmealDish.setDishCopies(d.getCopies());
                setmealDish.setDishId(d.getDishId());
                setmealDishList.add(setmealDish);
            });
        }
        setmeal.setDishList(null);
        return setmealService.add(setmeal,setmealDishList);
    }

    @ApiOperation(value = "修改套餐")
    @PutMapping("/update")
    public boolean update(@RequestBody SetmealVO setmealVo){
        Setmeal setmeal = setmealService.getById(setmealVo.getId());
        BeanUtils.copyProperties(setmealVo,setmeal);
        List<SetmealDish> setmealDishList = Lists.newArrayList();
        if(setmealVo.getDishList() != null){
            setmealVo.getDishList().forEach(d->{
                SetmealDish setmealDish = new SetmealDish();
                setmealDish.setIndex(0);
                setmealDish.setDishCopies(d.getCopies());
                setmealDish.setDishId(d.getDishId());
                setmealDishList.add(setmealDish);
            });
        }
        setmeal.setDishList(null);
        return setmealService.update(setmeal,setmealDishList);
    }

    /**
     * 删除套餐
     * @param ids
     * @return
     */
    @ApiOperation(value = "删除套餐")
    @DeleteMapping("/delete")
    public boolean delete(@RequestBody List<String> ids){
        return setmealService.removeByIds(ids);
    }

    /**
     * 设置售卖状态
     * @param saleStateVO
     * @return
     */
    @ApiOperation(value = "套餐停/起售")
    @PutMapping("/updateStatus")
    public boolean updateStatus(@RequestBody SaleStatusVO saleStateVO){
        if(SystemCode.FORBIDDEN==saleStateVO.getStatus()){
            return setmealService.forbiddenSalesStatus(saleStateVO.getIds());
        }else if(SystemCode.ENABLED==saleStateVO.getStatus()){
           return setmealService.enabledSalesStatus(saleStateVO.getIds());
        }
        return false;
    }


    @ApiOperation(value = "套餐分页")
    @GetMapping("/pageList/{page}/{pageSize}")
    public PageVO<Setmeal> queryByName(@PathVariable long page, @PathVariable long pageSize, @RequestParam(value = "name", defaultValue = "",required = false) String name){
        return new PageVO<>(setmealService.searchByName(name,page, pageSize));
    }
}
