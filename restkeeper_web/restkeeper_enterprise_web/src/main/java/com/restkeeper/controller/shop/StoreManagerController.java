package com.restkeeper.controller.shop;

import com.restkeeper.response.page.PageVO;
import com.restkeeper.shop.dto.StoreDTO;
import com.restkeeper.shop.entity.StoreManager;
import com.restkeeper.shop.service.IStoreManagerService;
import com.restkeeper.shop.service.IStoreService;
import com.restkeeper.vo.store.StoreManagerVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 门店管理员信息 前端控制器
 * </p>
 */
@Slf4j
@Api(tags = { "门店管理员" })
@RestController
@RequestMapping("/storeManager")
public class StoreManagerController{
	@Reference(version = "1.0.0", check=false)
	private IStoreManagerService storeManagerService;

	@Reference(version = "1.0.0", check=false)
	private IStoreService storeService;

	@ApiOperation(value = "获取门店省份信息")
	@GetMapping("/listProvince")
	@ResponseBody
	public List<String> listProvince() {
		return storeService.listAllProvince();
	}

	@ApiOperation(value = "根据省份获取门店列表")
	@GetMapping("/getStoreByProvince/{province}")
	@ResponseBody
	public List<StoreDTO> getStoreByProvince(@PathVariable String province) {
		return storeService.getStoreByProvince(province);
	}

	/**
	 * 添加店长
	 */
	@ApiOperation(value = "添加店长")
	@PostMapping(value = "/add")
	public boolean add(@RequestBody StoreManagerVO storeManagerVO) {
		return  storeManagerService.addStoreManager(
				storeManagerVO.getStoreManagerName(),
				storeManagerVO.getStoreManagerPhone(),
				storeManagerVO.getStoreIds());
	}


	/**
	 * 查询分页数据
	 */
	@ApiOperation(value = "查询分页数据")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "page", value = "当前页码", required = false, dataType = "Long"),
			@ApiImplicitParam(paramType = "path", name = "pageSize", value = "分大小", required = false, dataType = "Long"),
			@ApiImplicitParam(paramType = "query", name = "criteria", value = "店长姓名或者联系方式", required = false, dataType = "String") })
	@GetMapping(value = "/pageList/{page}/{pageSize}")
	public PageVO<StoreManager> findListByPage(@PathVariable Long page,
											   @PathVariable Long pageSize,
											   @RequestParam(defaultValue = "") String criteria) {
		return new PageVO<>(storeManagerService.page(criteria,page, pageSize));
	}

	/**
	 * 根据id查询
	 */
	@ApiOperation(value = "根据id查询数据")
	@ApiImplicitParam(paramType = "path", name = "pkId", value = "主键", required = true, dataType = "String")
	@GetMapping(value = "/{pkId}")
	public StoreManager getById(@PathVariable String pkId) {
		return storeManagerService.getById(pkId);
	}

	/**
	 * 删除门店管理员
	 */
	@ApiOperation(value = "删除数据")
	@DeleteMapping(value = "/del/{id}")
	public boolean delete(@PathVariable String id) {
		return storeManagerService.deleteStoreManager(id);
	}

	/**
	 * 停用启用
	 */
	@ApiOperation(value = "门店管理员停用,启用")
	@PutMapping(value = "/{id}/{status}")
	public boolean updateStatus(@PathVariable String id, @PathVariable int status) {
		return storeManagerService.updateStatus(id,status);
	}

	/**
	 * 门店管理员修改接口
	 */
	@ApiOperation(value = "更新数据")
	@PutMapping(value = "/update")
	public boolean update(@RequestBody StoreManagerVO storeManagerVO) {
		return storeManagerService.updateStoreManager(storeManagerVO.getStoreManagerId(),
				storeManagerVO.getStoreManagerName(),
				storeManagerVO.getStoreManagerPhone(),
				storeManagerVO.getStoreIds());
	}



}
