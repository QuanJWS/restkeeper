package com.restkeeper.controller.shop;

import com.restkeeper.response.page.PageVO;
import com.restkeeper.shop.dto.StoreDTO;
import com.restkeeper.shop.entity.Store;
import com.restkeeper.shop.service.IBrandService;
import com.restkeeper.shop.service.IStoreManagerService;
import com.restkeeper.shop.service.IStoreService;
import com.restkeeper.utils.Result;
import com.restkeeper.vo.shop.AddStoreVO;
import com.restkeeper.vo.shop.UpdateStoreVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 门店信息账号 前端控制器
 * </p>
 */
@Slf4j
@Api(tags = { "门店信息" })
@RestController
@RequestMapping("/store")
public class StoreController{
	@Reference(version = "1.0.0", check=false)
	private IStoreService storeService;

	@Reference(version = "1.0.0", check=false)
	private IBrandService brandService;

	@Reference(version = "1.0.0", check=false)
	private IStoreManagerService storeManagerService;


	@ApiOperation(value = "品牌列表（下拉选择使用）")
	@GetMapping("/brandList")
	@ResponseBody
	public List<Map<String, String>> list() {
		return brandService.brandList();
	}

	/**
	 * 分页数据
	 */
	@ApiOperation(value = "分页查询所有门店")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "page", value = "当前页码", required = true, dataType = "Integer"),
			@ApiImplicitParam(paramType = "path", name = "pageSize", value = "分大小", required = true, dataType = "Integer"),
			@ApiImplicitParam(paramType = "path", name = "name", value = "门店名称", required = false, dataType = "String") })
	@GetMapping(value = "/pageList/{page}/{pageSize}")
	public PageVO<Store> findListByPage(@PathVariable int page,
										@PathVariable int pageSize) {
		return new PageVO<Store>(storeService.queryPageByName(page, pageSize,null));
	}

	@GetMapping(value = "/search/{page}/{pageSize}/{name}")
	public PageVO<Store> search(@PathVariable Long page,
								@PathVariable Long pageSize,
								@PathVariable(required = false) String name){
		return new PageVO<Store>(storeService.queryPageByName(page, pageSize,name));
	}

	
	/**
	 * 根据id查询
	 */
	@ApiOperation(value = "根据id查询数据")
	@ApiImplicitParam(paramType = "query", name = "id", value = "主键", required = true, dataType = "String")
	@GetMapping(value = "/{id}")
	public Store getById(@PathVariable String id) {
		return storeService.getById(id);
	}

	/**
	 * 新增门店
	 */
	@ApiOperation(value = "新增数据")
	@PostMapping(value = "/add")
	public boolean add(@RequestBody AddStoreVO storeVO) {
		Store store = new Store();
		BeanUtils.copyProperties(storeVO, store);
		return storeService.save(store);
	}

	/**
	 * 删除
	 */
	@ApiOperation(value = "删除数据")
	@ApiImplicitParam(paramType = "query", name = "ids", value = "支持主键批量删除", required = true, dataType = "List")
	@DeleteMapping(value = "/del/{id}")
	public boolean delete(@PathVariable String id) {
		return storeService.removeById(id);
	}

	/**
	 * 修改
	 */
	@ApiOperation(value = "更新数据")
	@PutMapping(value = "/update")
	public boolean update(@RequestBody UpdateStoreVO updateVO) {
		Store store = new Store();
		BeanUtils.copyProperties(updateVO, store);
		return storeService.updateById(store);
	}

	/**
	 * 停业
	 */
	@ApiOperation(value = "停业/启动 接口")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "id", value = "主键", required = true, dataType = "String"),
			@ApiImplicitParam(paramType = "path", name = "status", value = "状态", required = true, dataType = "Integer")})
	@PutMapping(value = "/disabled/{id}/{status}")
	public boolean disabled(@PathVariable String id,@PathVariable Integer status) {
		Store store = storeService.getById(id);
		store.setStatus(status);
		return storeService.updateById(store);
	}

	@ApiOperation(value = "获取当前商户管理的门店信息")
	@GetMapping(value = "/listManagerStores")
	public List<StoreDTO> listManagerStores(){
		return storeService.listManagerStores();
	}

	@ApiOperation(value = "门店切换")
	@ApiImplicitParams({@ApiImplicitParam(paramType = "path", name = "storeId", value = "门店Id", required = false, dataType = "Integer")})
	@GetMapping(value = "/switchStore/{storeId}")
	public Result switchStore(@PathVariable String storeId){
		return storeManagerService.switchStore(storeId);
	}

}
