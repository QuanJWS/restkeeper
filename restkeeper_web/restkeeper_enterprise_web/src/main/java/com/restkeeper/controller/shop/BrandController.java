package com.restkeeper.controller.shop;

import com.restkeeper.response.page.PageVO;
import com.restkeeper.shop.entity.Brand;
import com.restkeeper.shop.service.IBrandService;
import com.restkeeper.vo.shop.AddTBrandVO;
import com.restkeeper.vo.shop.UpdateBrandVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

/**
    <p>
    * 品牌管理 前端控制器
   </p>
 */
@Slf4j
@Api(tags = {"品牌管理"})
@RestController
@RequestMapping("/brand")
public class BrandController{
    @Reference(version = "1.0.0", check=false)
    private IBrandService brandService;

    /**
     * 查询分页数据及统计信息
     */
    @ApiOperation(value = "分页查询所有品牌信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "page", value = "当前页码", required = false, dataType = "Long"),
            @ApiImplicitParam(paramType = "path", name = "pageSize", value = "分大小", required = false, dataType = "Long")})
    @GetMapping(value = "/pageList/{page}/{pageSize}")
    public PageVO<Brand> findListByPage(
            @PathVariable Long page, @PathVariable Long pageSize) {
        return new PageVO<>(brandService.queryPage(page, pageSize));
    }
    
    /**
     * 根据id查询
     */
    @ApiOperation(value = "根据id查询数据")
    @ApiImplicitParam(paramType="query", name = "id", value = "主键", required = true, dataType = "String")
    @GetMapping(value = "/getById/{id}")
    public Brand getById(@PathVariable String id){
       return brandService.getById(id);
    }
 
    /**
     * 新增品牌
     */
    @ApiOperation(value = "新增品牌")
    @PostMapping(value = "/add")
    public boolean add(@RequestBody AddTBrandVO brandVO){
    	Brand brand =new Brand();
    	BeanUtils.copyProperties(brandVO, brand);
        return brandService.save(brand);
    }
 
    /**
     * 删除
     */
    @ApiOperation(value = "删除数据")
    @ApiImplicitParam(paramType="query", name = "ids", value = "支持主键批量删除", required = true, dataType = "String")
    @DeleteMapping("/del")
    public boolean delete(@PathVariable String id){
          return brandService.removeById(id);
    }
 
    /**
     * 修改
     */
    @ApiOperation(value = "更新数据")
    @PutMapping("/update")
    public boolean update(@RequestBody UpdateBrandVO brandvo){
    	Brand tBrand = brandService.getById(brandvo.getBrandId());
    	BeanUtils.copyProperties(brandvo, tBrand);
        return brandService.updateById(tBrand);
    }

}
