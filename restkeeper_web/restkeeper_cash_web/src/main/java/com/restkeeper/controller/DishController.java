package com.restkeeper.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.lock.CalculationBusinessLock;
import com.restkeeper.response.page.PageVO;
import com.restkeeper.store.dto.AllDishDTO;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.DishFlavor;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.service.*;
import com.restkeeper.utils.RemarkType;
import com.restkeeper.vo.DishCategoryVO;
import com.restkeeper.vo.DishFlavorVO;
import com.restkeeper.vo.DishPanelVO;
import com.restkeeper.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(tags = {"菜品搜索相关接口"})
@RestController
@RequestMapping("/dish")
public class DishController {

    @Reference(version = "1.0.0", check=false)
    private IDishService dishService;

    @Reference(version = "1.0.0", check=false)
    private ISetmealService setmealService;

    @Reference(version = "1.0.0",check = false)
    IDishCategoryService dishCategoryService;

    @Reference(version = "1.0.0",check = false)
    ISetmealDishService setmealDishService;

    @Reference(version = "1.0.0", check=false)
    private ISellCalculationService sellCalculationService;

    @Autowired
    private CalculationBusinessLock lock;

    @Reference(version = "1.0.0", check=false)
    private IRemarkService remarkService;

    @Reference(version = "1.0.0", check=false)
    private IDishFlavorService dishFlavorService;

    /**
     * 获取套餐和产品分类类别
     * @return
     */
    @ApiOperation(value = "菜品搜索 categoryId name")
    @GetMapping("/category")
    public List<DishCategoryVO> getCategory(){
        return  dishCategoryService
                .getAllCategory()
                .stream()
                .map(d->{
                    DishCategoryVO vo = new DishCategoryVO();
                    vo.setCategoryId(d.getCategoryId());
                    vo.setName(d.getName());
                    vo.setType(d.getType());
                    return vo;
                })
                .collect(Collectors.toList());
    }


    /**
     * 备注
     */
    @ApiOperation(value = "备注")
    @GetMapping("/remark/type/{type}")
    public List<String> orderRemark(@PathVariable Integer type){
       return remarkService.getRemark(RemarkType.getRemark(type));
    }


    /**
     * 根据菜品id口味
     */
    @ApiOperation(value = "根据菜品id获取口味信息")
    @GetMapping("/flavor/{dishId}")
    public List<DishFlavorVO> dishFlavor(@PathVariable String dishId){
        List<DishFlavor> dishFlavors= dishFlavorService.getFlavor(dishId);
        List<DishFlavorVO> dishFlavorVOList =new ArrayList<>();
        if(dishFlavors == null || dishFlavors.size() <= 0) return dishFlavorVOList;
        dishFlavors.forEach(d->{
            DishFlavorVO dishFlavorVO=new DishFlavorVO();
            dishFlavorVO.setFlavor(d.getFlavorName());
            String flavorValue = d.getFlavorValue();
            String remarkValue_substring=flavorValue.substring(flavorValue.indexOf("[")+1,flavorValue.indexOf("]"));
            if(StringUtils.isNotEmpty(remarkValue_substring)){
                String[] flavor_array= remarkValue_substring.split(",");
                dishFlavorVO.setFlavorData(Arrays.asList(flavor_array));
            }
            dishFlavorVOList.add(dishFlavorVO);
        });
        return dishFlavorVOList;
    }

    @ApiOperation(value = "根据菜品分类id获取菜品信息")
    @GetMapping("/dishPageList/{categoryId}/{type}/{page}/{pageSize}")
    public PageVO<DishVO> getDishByCategory(@PathVariable String categoryId,
                                            @PathVariable int type,
                                            @PathVariable long page,
                                            @PathVariable long pageSize){
        PageVO<DishVO> result = new PageVO<>();
        if(type == 0){
           IPage<AllDishDTO> dishPage = dishService.selectAllDish(page,pageSize);
            result.setPages(dishPage.getPages());
            result.setPage(dishPage.getCurrent());
            result.setPagesize(dishPage.getSize());
            result.setCounts(dishPage.getTotal());
            result.setItems(dishPage
                    .getRecords()
                    .stream()
                    .map(s->{
                        DishVO dishVO = new DishVO();
                        dishVO.setDishId(s.getId());
                        dishVO.setDishName(s.getName());
                        dishVO.setPrice(s.getPrice());
                        dishVO.setType(s.getType());
                        dishVO.setRemainder(sellCalculationService.getRemainderCount(s.getId()));
                        return  dishVO;
                    }).collect(Collectors.toList())
            );
            return result;
        }else if(type == 1){
            IPage<Dish> dishPage = dishService.queryByCategory(categoryId,page,pageSize);
            result.setPages(dishPage.getPages());
            result.setPage(dishPage.getCurrent());
            result.setPagesize(dishPage.getSize());
            result.setCounts(dishPage.getTotal());
            result.setItems(dishPage
                    .getRecords()
                    .stream()
                    .map(d->{
                        DishVO dishVO = new DishVO();
                        dishVO.setDishId(d.getId());
                        dishVO.setDishName(d.getName());
                        dishVO.setPrice(d.getPrice());
                        dishVO.setType(1);
                        dishVO.setRemainder(sellCalculationService.getRemainderCount(d.getId()));
                        return  dishVO;
                    }).collect(Collectors.toList())
            );
            return result;
        }else if(type == 2){
            IPage<Setmeal> dishPage = setmealService.queryByCategory(categoryId,page,pageSize);
            result.setPages(dishPage.getPages());
            result.setPage(dishPage.getCurrent());
            result.setPagesize(dishPage.getSize());
            result.setCounts(dishPage.getTotal());
            result.setItems(dishPage
                    .getRecords()
                    .stream()
                    .map(s->{
                        DishVO dishVO = new DishVO();
                        dishVO.setDishId(s.getId());
                        dishVO.setDishName(s.getName());
                        dishVO.setPrice(s.getPrice());
                        dishVO.setType(2);
                        dishVO.setRemainder(sellCalculationService.getRemainderCount(s.getId()));
                        return  dishVO;
                    }).collect(Collectors.toList())
            );
            return result;
        }
        throw new BussinessException("请选择正确的分类");
    }


    @ApiOperation(value = "根据code获取菜品信息")
    @GetMapping("/queryByCode/{code}/{page}/{pageSize}")
    public PageVO<DishPanelVO> searchByCode(@PathVariable String code,
                                       @PathVariable int page,
                                       @PathVariable int pageSize) {

        //TODO:使用ES 搜索菜品
        return new PageVO<>();
    }

    @ApiOperation(value = "查询可用的菜品列表")
    @GetMapping("/findEnableDishList/{categoryId}")
    public List<Map<String,Object>> findEnableDishList(@PathVariable String categoryId,
                                                       @RequestParam(value = "name",defaultValue = "") String name){
        return dishService.findEnableDishListInfo(categoryId, name);
    }

}
