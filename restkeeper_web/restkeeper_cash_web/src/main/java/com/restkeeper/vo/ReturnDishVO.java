package com.restkeeper.vo;

import lombok.Data;

/**
 * 退菜请求
 */
@Data
public class ReturnDishVO{
    private String orderId;
    private String dishId;
    private String remark;
    private Integer price;
    private Integer dishType;
    private String dishName;
}
