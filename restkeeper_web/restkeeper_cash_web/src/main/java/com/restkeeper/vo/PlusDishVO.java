package com.restkeeper.vo;

import com.restkeeper.dto.DishDTO;
import lombok.Data;

import java.util.List;

/**
 * 加菜
 */
@Data
public class PlusDishVO {
    private String orderId;
    private Integer payAmmount;
    private OrderDetailVO orderDetailVO;
}
