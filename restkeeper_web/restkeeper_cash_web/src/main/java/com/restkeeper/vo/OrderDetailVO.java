package com.restkeeper.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 订单菜品列表
 */
@Data
public class OrderDetailVO {

    @ApiModelProperty(value = "订单id")
    private String detailId;

    @ApiModelProperty(value = "菜品或者套餐id")
    private  String dishId;
    @ApiModelProperty(value = "名称")
    private  String dishName;
    @ApiModelProperty(value = "类型 1 菜品 2 套餐")
    private  Integer type;
    @ApiModelProperty(value = "单价")
    private  Integer price;
    @ApiModelProperty(value = "份数")
    private Integer dishNumber;
    @ApiModelProperty(value = "状态")
    private Integer status=1; //状态 1正常 2 赠菜 3 退菜  4 加菜
    @ApiModelProperty(value = "口味")
    private List<String> flavorList = Lists.newArrayList();
    @ApiModelProperty(value = "赠菜备注")
    private List<String> presentRemark= Lists.newArrayList();
    @ApiModelProperty(value = "赠菜备注")
    private List<String> returnRemark = Lists.newArrayList();

}
