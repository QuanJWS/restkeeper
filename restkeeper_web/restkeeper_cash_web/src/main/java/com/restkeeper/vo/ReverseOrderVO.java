package com.restkeeper.vo;

import lombok.Data;

@Data
public class ReverseOrderVO {
    private String orderId;
    private String dishName;
    private double dishPrice;
}
