package com.restkeeper.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

/**
 * 结账VO
 */
@Data
public class PayVO {
    @ApiModelProperty(value = "支付方式")
    private Integer payType; //付款方式 0 免单 1 现金 2 微信 3 支付宝  4 银行卡
    @ApiModelProperty(value = "实付金额")
    private Integer payAmount; //实付金额
    @ApiModelProperty(value = "抹零金额")
    private Integer smallAmount; //抹零金额"
}
