package com.restkeeper.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateEnterpiseAccountVO extends AddEnterpiseAccountVO{

	@ApiModelProperty(value = "企业id")
	private String enterpriseId;

	
}
