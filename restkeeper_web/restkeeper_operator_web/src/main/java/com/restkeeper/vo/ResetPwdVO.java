package com.restkeeper.vo;

import lombok.Data;

@Data
public class ResetPwdVO{
    private String id;
    private String pwd;
}
