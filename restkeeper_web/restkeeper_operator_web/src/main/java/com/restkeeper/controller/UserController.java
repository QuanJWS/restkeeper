package com.restkeeper.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.entity.OperatorUser;
import com.restkeeper.service.IOperatorUserService;
import com.restkeeper.vo.LoginVO;
import com.restkeeper.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 管理员的登录接口
 */
@Api(tags = {"运营端管理员相关接口"})
@RestController
@RefreshScope
public class UserController{

    @Reference(version = "1.0.0", check=false)
    private IOperatorUserService operatorUserService;

    @Value("${server.port}")
    private String port;

    @Value("${key:''}")
    private String key;

    @GetMapping(value = "/echo/{message}")
    public String echo(@PathVariable(value = "message") String message) {
        return "Hello Nacos Discovery " + message + ", i am from port " + port;
    }

    @GetMapping(value = "/config")
    public String config() {
        return "Hello Nacos Config get "+key ;
    }

    @GetMapping(value = "/list")
    public List<OperatorUser>  pageList(){
        return operatorUserService.list();
    }

    @ApiOperation(value = "分页列表查询")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="path", name = "page", value = "当前页码", required = false, dataType = "Integer"),
            @ApiImplicitParam(paramType="path", name = "pageSize", value = "分大小", required = false, dataType = "Integer"),
            @ApiImplicitParam(paramType="path", name = "name", value = "用户名", required = false, dataType = "String")
    })
    @GetMapping(value = "/pageList/{page}/{pageSize}/{name}")
    public IPage<OperatorUser> findListByPage(@PathVariable int page, @PathVariable int pageSize,@PathVariable String name){
        return operatorUserService.queryPageByName( page,  pageSize,  name);
    }

    @ApiOperation(value = "登录校验")
    @PostMapping("/login")
    public Result login(@RequestBody LoginVO loginVO){
         return operatorUserService.login(loginVO.getLoginName(), loginVO.getLoginPass());
    }

}
